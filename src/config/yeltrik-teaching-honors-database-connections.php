<?php

use Illuminate\Support\Str;

return

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    [
        // Connection
        'teaching_honors' => [
            'driver' => 'sqlite',
            'url' => env('DATABASE_URL'),
            'database' => env('TEACHING_HONORS_DB_DATABASE', database_path('teaching_honors.sqlite')),
            'prefix' => '',
            'foreign_key_constraints' => env('DB_FOREIGN_KEYS', true),
        ],

        // SQLITE
        'teaching_honors_sqlite_example' => [
            'driver' => 'sqlite',
            'url' => env('DATABASE_URL'),
            'database' => env('TEACHING_HONORS_DB_DATABASE', database_path('teaching_honors.sqlite')),
            'prefix' => '',
            'foreign_key_constraints' => env('DB_FOREIGN_KEYS', true),
        ],

        // MYSQL
        'teaching_honors_mysql_example' => [
            'driver' => 'mysql',
            'url' => env('DATABASE_URL'),
            'host' => env('TEACHING_HONORS_DB_HOST', '127.0.0.1'),
            'port' => env('TEACHING_HONORS_DB_PORT', '3306'),
            'database' => env('TEACHING_HONORS_DB_DATABASE', 'teaching_honors'),
            'username' => env('TEACHING_HONORS_DB_USERNAME', 'teaching_honors'),
            'password' => env('TEACHING_HONORS_DB_PASSWORD', ''),
            'unix_socket' => env('TEACHING_HONORS_DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
            ]) : [],
        ],

    ];
