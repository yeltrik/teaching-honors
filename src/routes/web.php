<?php

use Illuminate\Support\Facades\Route;

Route::prefix('/import')->group(function () {
    Route::prefix('/teaching-honors')->group(function () {
        Route::get('/', [\Yeltrik\TeachingHonors\app\http\controllers\ImportTeachingHonorsController::class, 'index'])->name('imports.teaching-honors.index');
        Route::post('/', [\Yeltrik\TeachingHonors\app\http\controllers\ImportTeachingHonorsController::class, 'store'])->name('imports.teaching-honors.store');
        Route::get('/create', [\Yeltrik\TeachingHonors\app\http\controllers\ImportTeachingHonorsController::class, 'create'])->name('imports.teaching-honors.create');
        Route::get('/faculty', [\Yeltrik\TeachingHonors\app\http\controllers\ImportTeachingHonorsController::class, 'faculty'])->name('imports.teaching-honors.faculty');
    });
});

Route::prefix('/mail')->group(function () {
    Route::prefix('/teaching-honors')->group(function () {
        Route::prefix('/department')->group(function () {
            Route::prefix('/nominee')->group(function () {
                Route::prefix('/nomination')->group(function () {
                    Route::get('/', [\Yeltrik\TeachingHonors\app\http\controllers\DepartmentNomineesNominatedController::class, 'index'])->name('mails.teaching-honors.departments.nominees.nominated.index');
                    Route::get('/preview', [\Yeltrik\TeachingHonors\app\http\controllers\DepartmentNomineesNominatedController::class, 'preview'])->name('mails.teaching-honors.departments.nominees.nominated.preview');
                    Route::get('/mail', [\Yeltrik\TeachingHonors\app\http\controllers\DepartmentNomineesNominatedController::class, 'mail'])->name('mails.teaching-honors.departments.nominees.nominated.mail');
                });
            });
        });
        Route::prefix('/nominee')->group(function () {
            Route::prefix('/nominated')->group(function () {
                Route::get('/', [\Yeltrik\TeachingHonors\app\http\controllers\NomineeNominatedController::class, 'index'])->name('mails.teaching-honors.nominees.nominated.index');
                Route::get('/preview', [\Yeltrik\TeachingHonors\app\http\controllers\NomineeNominatedController::class, 'preview'])->name('mails.teaching-honors.nominees.nominated.preview');
                Route::get('/mail', [\Yeltrik\TeachingHonors\app\http\controllers\NomineeNominatedController::class, 'mail'])->name('mails.teaching-honors.nominees.nominated.mail');
            });
        });
    });
});

Route::prefix('/report')->group(function () {
    Route::prefix('/teaching-honors')->group(function () {
        Route::get('/', [\Yeltrik\TeachingHonors\app\http\controllers\ReportController::class, 'index'])->name('reports.teaching-honors.index');
        Route::prefix('/college')->group(function () {
            Route::get('/', [\Yeltrik\TeachingHonors\app\http\controllers\ReportCollegeController::class, 'index'])->name('reports.teaching-honors.colleges.index');
            Route::get('/{college}', [\Yeltrik\TeachingHonors\app\http\controllers\ReportCollegeController::class, 'show'])->name('reports.teaching-honors.colleges.show');
        });
        Route::prefix('/department')->group(function () {
            Route::get('/{department}', [\Yeltrik\TeachingHonors\app\http\controllers\ReportDepartmentController::class, 'show'])->name('reports.teaching-honors.departments.show');
        });
        Route::prefix('/term')->group(function () {
            Route::get('/', [\Yeltrik\TeachingHonors\app\http\controllers\ReportTermController::class, 'index'])->name('reports.teaching-honors.terms.index');
            Route::get('/{term}', [\Yeltrik\TeachingHonors\app\http\controllers\ReportTermController::class, 'show'])->name('reports.teaching-honors.terms.show');
        });
    });
});

Route::prefix('/teaching-honors')->group(function () {
    Route::prefix('nomination')->group(function () {
        Route::post('/', [\Yeltrik\TeachingHonors\app\http\controllers\NominationController::class, 'store'])->name('teaching-honors.nominations.store');
        Route::get('/form', [\Yeltrik\TeachingHonors\app\http\controllers\NominationController::class, 'create'])->name('teaching-honors.nominations.create');
        Route::get('/{nomination}', [\Yeltrik\TeachingHonors\app\http\controllers\NominationController::class, 'show'])->name('teaching-honors.nominations.show');
    });
    Route::prefix('/nominator')->group(function () {
        Route::get('/', [\Yeltrik\TeachingHonors\app\http\controllers\NominatorController::class, 'index'])->name('teaching-honors.nominators.index');
    });
    Route::prefix('/nominee')->group(function () {
        Route::get('/', [\Yeltrik\TeachingHonors\app\http\controllers\NomineeController::class, 'index'])->name('teaching-honors.nominees.index');
        Route::get('/lookup/{fullName}', [\Yeltrik\TeachingHonors\app\http\controllers\NomineeController::class, 'lookup'])->name('teaching-honors.nominees.lookup');
        Route::get('/{nomineeFrom}/nomination/transfer/{nomineeTo}', [\Yeltrik\TeachingHonors\app\http\controllers\NomineeController::class, 'transfer'])->name('teaching-honors.nominees.nominations.transfer');
        Route::prefix('/{nominee}')->group(function () {
            Route::get('/', [\Yeltrik\TeachingHonors\app\http\controllers\NomineeController::class, 'show'])->name('teaching-honors.nominees.show');
            Route::delete('/', [\Yeltrik\TeachingHonors\app\http\controllers\NomineeController::class, 'destroy'])->name('teaching-honors.nominees.destroy');
        });
    });
    Route::prefix('/user')->group(function () {
        Route::get('/{user}/nomination', [\Yeltrik\TeachingHonors\app\http\controllers\UserNominationController::class, 'index'])->name('teaching-honors.users.nominations.index');
    });
});
