<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNominationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('teaching_honors')->create('nominations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('term_id');
            $table->unsignedBigInteger('nominee_id');
            $table->unsignedBigInteger('nominator_id');
            $table->unsignedBigInteger('example_id');
            $table->timestamps();

            $table->unique(['term_id', 'nominee_id', 'nominator_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('teaching_honors')->dropIfExists('nominations');
    }
}
