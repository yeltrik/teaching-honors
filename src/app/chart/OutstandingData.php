<?php

namespace Yeltrik\TeachingHonors\app\chart;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\HigherOrderBuilderProxy;
use Yeltrik\Color\app\Colors;
use Yeltrik\Profile\app\models\Email;
use Yeltrik\Profile\app\models\PersonalName;
use Yeltrik\Profile\app\models\Profile;
use Yeltrik\TeachingHonors\app\models\Nomination;
use Yeltrik\TeachingHonors\app\models\Nominee;
use Yeltrik\UniTrm\app\models\Term;

class OutstandingData
{

    private ?Term $term = NULL;

    /**
     * @param array $dataset
     * @return array
     */
    private static function backgroundColor(array $dataset)
    {
        return Colors::fromArrayByKeys($dataset)->generateRGBAColors();
    }

    /**
     * @param array $dataset
     * @return array
     */
    private static function data(array $dataset)
    {
        return array_values($dataset);
    }

    /**
     * @return array
     */
    public function datasets(): array
    {
        // Init
        $datasets = [];
        $dataset = static::newDataset();
        foreach ($this->nominees() as $nominee) {
            if (static::timeForNewDataset($nominee)) {
                static::pushCurrentDataset($datasets, $dataset);
                $dataset = static::newDataset();
            }
            static::pushNomineeToDataset($dataset, $nominee);
        }
        static::pushCurrentDataset($datasets, $dataset);
        return $datasets;
    }

    /**
     * @param Term $term
     */
    public function forTerm(Term $term)
    {
        $this->term = $term;
    }

    /**
     * @param Nominee $nominee
     * @return HigherOrderBuilderProxy|mixed
     */
    private static function keyFromNominee(Nominee $nominee)
    {
        $profile = $nominee->profile;
        if ( $profile instanceof Profile ) {
            $personalName = $profile->personalNames()->first();
            if ( $personalName instanceof PersonalName ) {
                return $personalName->fullName;
            }
            $email = $profile->emails()->first();
            if ( $email instanceof Email ) {
                return $email->email;
            }
            return $nominee->id;
        } else {
            return $nominee->id;
        }
    }

    /**
     * @return array
     */
    public function labels(): array
    {
        $labels = [];
        foreach ($this->nominees() as $nominee) {
            $labels[] = static::keyFromNominee($nominee);
        }
        return $labels;
    }

    /**
     * @return array
     */
    private static function newDataset()
    {
        return [];
    }

    /**
     * @return Builder[]|Collection
     */
    private function nominees()
    {
        return $this->nomineeQuery()
            ->get();
    }

    /**
     * @return Builder
     */
    private function nomineeQuery()
    {
        $query = Nominee::query();

        $term = $this->term();
        if ($term instanceof Term) {
            static::whereInNominationsForTerm($query, $term);

            $termId = $term->id;
            $query->withCount(['nominations' => function (Builder $query) use ($termId) {
                $query->where('term_id', '=', $termId);
            }]);
        } else {
            $query->withCount('nominations');
        }

        $query->orderBy('nominations_count', 'asc');

        return $query;
    }

    /**
     * @param int $padSize
     * @param int $value
     * @return array
     */
    private static function pad(int $padSize, $value = 0)
    {
        return array_pad([], $padSize, $value);
    }

    /**
     * @param array $datasets
     * @param array $dataset
     */
    private static function padDataset(array $datasets, array &$dataset)
    {
        $dataset = array_merge(static::pad(static::padSize($datasets)), $dataset);
    }

    /**
     * @param array $datasets
     * @return int
     */
    private static function padSize(array $datasets): int
    {
        $lastDataset = array_pop($datasets);
        if (!empty($lastDataset)) {
            return sizeof($lastDataset['data']);
        } else {
            return 0;
        }
    }

    /**
     * @param array $datasets
     * @param array $dataset
     */
    private static function pushCurrentDataset(array &$datasets, array $dataset)
    {
        $datasetCopy = $dataset;
        static::padDataset($datasets, $datasetCopy);
        $datasets[] = [
            'data' => static::data($datasetCopy),
            'backgroundColor' => static::backgroundColor($datasetCopy)
        ];
    }

    /**
     * @param array $dataset
     * @param Nominee $nominee
     */
    private static function pushNomineeToDataset(array &$dataset, Nominee $nominee)
    {
        $dataset[static::keyFromNominee($nominee)] = $nominee->nominations_count;
    }

    /**
     * @return Term|null
     */
    private function term(): ?Term
    {
        return $this->term;
    }

    private static ?Nominee $lastNominee = NULL;

    /**
     * @param Nominee $nominee
     * @return bool
     */
    private static function timeForNewDataset(Nominee $nominee): bool
    {
        if (static::$lastNominee instanceof Nominee) {
            if ($nominee->nominations_count != static::$lastNominee->nominations_count) {
                static::$lastNominee = $nominee;
                return TRUE;
            } else {
                static::$lastNominee = $nominee;
                return FALSE;
            }
        } else {
            static::$lastNominee = $nominee;
            return false;
        }
    }

    /**
     * @param Builder $query
     * @param Term $term
     */
    private static function whereInNominationsForTerm(Builder &$query, Term $term)
    {
        $query->whereIn('id', Nomination::query()
            ->where('term_id', '=', $term->id)
            ->pluck('nominee_id')
            ->toArray()
        );
    }

}

