<?php

namespace Yeltrik\TeachingHonors\app\http\requests;

use Illuminate\Foundation\Http\FormRequest;
use Yeltrik\TeachingHonors\app\models\Nomination;

/**
 * Class NominationStoreRequest
 *
 * @property ?int term_id
 * @property ?int nominee_id
 * @property ?string nominee_lookup_method
 *
 * @package App\Http\Requests
 */
class NominationStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('create', Nomination::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        // Standard Rules
        $rules = array_merge($rules, [
            'term_id' => 'required',
            'nomination_example' => 'required',
            'nominee_lookup_method' => 'required',
            'nominator_profile_id' => 'required',
            "reason_ids" => 'required|array|min:1',
        ]);

        // Custom Rules based on the Nominee Lookup Method
        $nomineeLookupMethod = $this->nominee_lookup_method;
        switch ($nomineeLookupMethod) {
            case "true" :
                $rules = array_merge($rules, [
                    "nominee_id" => 'required',
                ]);
                break;
            case "false" :
                $rules = array_merge($rules, [
                    "nominee_first_name" => 'required',
                    "nominee_last_name" => 'required',
                    "nominee_email" => 'required',
                    "nominee_college" => 'required',
                    "nominee_department" => 'required'
                ]);
                break;
            default:
                dd('Unsupported Nominee Lookup Method Value');
                break;
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'nomination_example.required' => 'Provide a example of ways this Nominee has helped you be successful in your course',
            'reason_ids.required' => 'You should select at least one reason.'
        ];
    }
}
