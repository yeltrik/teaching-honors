<?php

namespace Yeltrik\TeachingHonors\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Yeltrik\Profile\app\models\PersonalName;
use Yeltrik\Profile\app\models\Profile;
use Yeltrik\TeachingHonors\app\models\Nomination;
use Yeltrik\TeachingHonors\app\models\Nominee;

class NomineeController extends Controller
{

    /**
     * NominationController constructor.
     */
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Nominee::class);
        $nominees = Nominee::query()->get();

        return view('teachingHonors::nominee.index', compact([
            'nominees'
        ]));
    }

    public function show(Nominee $nominee)
    {
        $this->authorize('view', $nominee);

        $personalNames = $nominee->profile->personalNames;
        $emails = $nominee->profile->emails;

        $nominations = Nomination::query()
            ->where('nominee_id', '=', $nominee->id)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('teachingHonors::nominee.show', compact([
            'nominee',
            'personalNames',
            'emails',
            'nominations'
        ]));
    }

    /**
     * @param Nominee $nominee
     */
    public function destroy(Nominee $nominee)
    {
        $this->authorize('destroy', $nominee);

        if ( $nominee->nominations->count() === 0 ) {
            $nominee->delete();

            return back()->with([
                'flash_message' => 'Deleted',
                'flash_message_important' => false
            ]);
        } else {
            dd('Unable to destroy a Nominee that has Nominations');
        }
    }

    public function lookup(string $fullName)
    {
        $parts = explode(" ", $fullName);
        $first = array_shift($parts);
        $last = implode(" ", $parts);
        $nominee = Nominee::query()
            ->whereIn('profile_id', Profile::query()
                ->whereIn('id', PersonalName::query()
                    ->where('first', '=', $first)
                    ->where('last', '=', $last)
                    ->pluck('profile_id')
                    ->toArray()
                )
                ->pluck('id')
                ->toArray()
            );

        if ($nominee->count() === 1) {
            return redirect()->route('teaching-honors.nominees.show', $nominee->first());
        } else {
            $parts = explode(" ", $fullName);
            $nominees = Nominee::query()
                ->whereIn('profile_id', Profile::query()
                    ->whereIn('id', PersonalName::query()
                        ->orWhereIn('first', $parts)
                        ->orWhereIn('last', $parts)
                        ->pluck('profile_id')
                        ->toArray()
                    )
                    ->pluck('id')
                    ->toArray()
                )
                ->get();
            return view('teachingHonors::nominee.lookup', compact('nominees'));
        }
    }

    /**
     * @param Nominee $nomineeFrom
     * @param Nominee $nomineeTo
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function transfer(Nominee $nomineeFrom, Nominee $nomineeTo)
    {
        $this->authorize('transferAny', Nominee::class);

        foreach($nomineeFrom->nominations as $nomination) {
            $nomination->nominee()->associate($nomineeTo);
            $nomination->save();
        }

        return redirect()->route('teaching-honors.nominees.show', $nomineeTo);
    }


}
