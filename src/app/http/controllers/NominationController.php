<?php

namespace Yeltrik\TeachingHonors\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Yeltrik\Profile\app\import\ProfileImporter;
use Yeltrik\Profile\app\models\Email;
use Yeltrik\Profile\app\models\PersonalName;
use Yeltrik\Profile\app\models\Profile;
use Yeltrik\TeachingHonors\app\form\NominatorInfo;
use Yeltrik\TeachingHonors\app\form\NomineeSelectOptions;
use Yeltrik\TeachingHonors\app\form\ReasonRadios;
use Yeltrik\TeachingHonors\app\http\requests\NominationStoreRequest;
use Yeltrik\TeachingHonors\app\models\Nomination;
use Yeltrik\TeachingHonors\app\models\Nominee;
use Yeltrik\UniOrg\app\models\College;
use Yeltrik\UniOrg\app\models\Department;

class NominationController extends Controller
{

    /**
     * NominationController constructor.
     */
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @param Nomination $nomination
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function show(Nomination $nomination)
    {
        $this->authorize('view', $nomination);

        $term = $nomination->term;
        $personalName = $nomination->nominee->profile->personalNames->first();
        if ($personalName instanceof PersonalName) {
            $nomineeName = $personalName->fullName;
        } else {
            $email = $nomination->nominee->profile->emails->first();
            if ( $email instanceof Email ) {
                $nomineeName = $email->email;
            } else {
                $nomineeName = "Unknown";
            }
        }

        return view('teachingHonors::nomination.show', compact([
            'nomination',
            'term',
            'nomineeName'
        ]));
    }

    /**
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Nomination::class);

        $nomineeSelectOptions = new NomineeSelectOptions();
        //$nomineeSelectOptions->facultyOnly(false);

        $colleges = College::query()->orderBy('name', 'asc')->get();
        $departments = Department::query()->orderBy('name', 'asc')->get();
        $reasonRadios = new ReasonRadios();
        $nominatorInfo = new NominatorInfo();

        $nominatorProfile = $nominatorInfo->profile();
        $nominatorFirstName = $nominatorInfo->firstName();
        $nominatorLastName = $nominatorInfo->lastName();
        $nominatorEmail = $nominatorInfo->email();
        $nominatorRelationship = '';

        return view('teachingHonors::nomination.create', compact(
            'nomineeSelectOptions',
            'colleges', 'departments',
            'reasonRadios',
            'nominatorProfile',
            'nominatorFirstName', 'nominatorLastName', 'nominatorEmail', 'nominatorRelationship'
        ));
    }

    /**
     * @param NominationStoreRequest $request
     * @return RedirectResponse
     */
    public function store(NominationStoreRequest $request)
    {
        $nomineeId = $request->nominee_id;
        if ($nomineeId != NULL) {
            return $this->storeByNomineeLookup($request, $nomineeId);
        } else {
            return $this->storeByNomineeManual($request);
        }
    }

    /**
     * @param NominationStoreRequest $request
     * @param int $nomineeId
     * @return RedirectResponse
     */
    private function storeByNomineeLookup(NominationStoreRequest $request, int $nomineeId)
    {
        $nominee = Nominee::query()
            ->where('id', '=', $nomineeId)
            ->first();
        if ($nominee instanceof Nominee) {
            return (new NomineeNominationController())->store($nominee, $request);
        } else {
            return back()->withInput($request->all());
        }
    }

    /**
     * @param NominationStoreRequest $request
     * @return RedirectResponse
     */
    private function storeByNomineeManual(NominationStoreRequest $request)
    {
        $nomineeFirstName = $request->nominee_first_name;
        $nomineeLastName = $request->nominee_last_name;
        $nomineeEmail = $request->nominee_email;

        // TODO: Do something with College / Department...

        $profileImporter = new ProfileImporter();
        $profileImporter->associateWithEmailStr($nomineeEmail);
        $profileImporter->associateWithPersonalNameStr($nomineeFirstName, $nomineeLastName);
        $profile = $profileImporter->profile();

        // Use Profile to Load / Create a Nominee
        if ($profile instanceof Profile) {
            $nominee = Nominee::query()
                ->where('profile_id', '=', $profile->id)
                ->first();
            if ($nominee instanceof Nominee === FALSE) {
                $nominee = new Nominee();
                $nominee->profile()->associate($profile);
                $nominee->save();
            }
            return (new NomineeNominationController())->store($nominee, $request);
        } else {
            dd('Must have a Profile to load/create a nominee');
        }
        return back()->withInput($request->all());
    }

}
