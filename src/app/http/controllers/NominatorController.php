<?php

namespace Yeltrik\TeachingHonors\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Yeltrik\Profile\app\models\PersonalName;
use Yeltrik\Profile\app\models\Profile;
use Yeltrik\TeachingHonors\app\models\Nomination;
use Yeltrik\TeachingHonors\app\models\Nominator;
use Yeltrik\TeachingHonors\app\models\Nominee;

class NominatorController extends Controller
{

    /**
     * NominationController constructor.
     */
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Nominator::class);
        $nominators = Nominator::query()
            ->whereIn('id', Nomination::query()
                ->pluck('nominator_id')
                ->toArray()
            )
            ->inRandomOrder()
            ->get();

        return view('teachingHonors::nominators.index', compact([
            'nominators'
        ]));
    }

}
