<?php

namespace Yeltrik\TeachingHonors\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Yeltrik\ImportProfileAsanaUniMbr\app\models\ProfileUniMbr;
use Yeltrik\Profile\app\models\Profile;
use Yeltrik\TeachingHonors\app\import\NominationImporter;
use Yeltrik\TeachingHonors\app\models\Nominee;
use Yeltrik\UniMbr\app\models\Faculty;
use Yeltrik\UniMbr\app\models\Member;

class ImportTeachingHonorsController extends Controller
{

    /**
     * ImportTeachingHonorsController constructor.
     */
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('import');

        return view('teachingHonors::import.teaching-honors.index');
    }

    /**
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('import');
        return view('teachingHonors::import.teaching-honors.create');
    }

    public function faculty()
    {
        // Find Faculty without Nominees
        $facultyWithoutProfile = Faculty::query()
            ->whereNotIn('member_id', ProfileUniMbr::query()
                ->pluck('member_id')
                ->toArray()
            );

        foreach ($facultyWithoutProfile->get() as $faculty) {
            dd([
                $faculty,
                $faculty->member,
            ]);
        }

        $facultyWithoutNominee = Faculty::query()
            ->whereIn('member_id', ProfileUniMbr::query()
                ->whereIn('profile_id', Profile::query()
                    ->whereNotIn('id', Nominee::query()
                        ->pluck('profile_id')
                        ->toArray()
                    )
                    ->pluck('id')
                    ->toArray()
                )
                ->pluck('member_id')
                ->toArray()
            );

        //dd($facultyWithoutNominee->count());

        foreach ($facultyWithoutNominee->get() as $faculty) {
            $profile = Profile::query()
                ->whereIn('id', ProfileUniMbr::query()
                    ->where('member_id', '=', $faculty->member_id)
                    ->pluck('profile_id')
                    ->toArray()
                )
                ->first();

//            dd([
//                $faculty,
//                $faculty->member,
//                $profile
//            ]);

            if (
                !Nominee::query()
                ->where('profile_id', '=', $profile->id)
                ->exists()
            ) {
                $nominee = new Nominee();
                $nominee->profile()->associate($profile);
                $nominee->save();
            }

        }

        dd('done');
        return view('teachingHonors::import.teaching-honors.index');
    }

    /**
     * @param Request $request
     * @throws AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('import');
        $request->validate([
            //'import_method' => 'required',
        ]);

        switch ($request->import_method) {
            case "export_json":
                NominationImporter::validate($request);
                $teachingHonorsImporter = new NominationImporter($request);
                return $teachingHonorsImporter->process();
                break;
            default:
                dd('Unsupported Import Method: ' . $request->import_method);
        }
    }
}
