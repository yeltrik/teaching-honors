<?php

namespace Yeltrik\TeachingHonors\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Yeltrik\UniOrg\app\models\College;

class ReportCollegeController extends Controller
{

    /**
     * ReportPdController constructor.
     */
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        // TODO: Gate to Check if User can report for Any College
        // Admin
        $this->authorize('viewAny', College::class);

        $colleges = College::query()->get();

        return view('teachingHonors::report.teaching-honors.college.index', compact(
            'colleges'
        ));
    }

    /**
     * @param College $college
     * @return Application|Factory|View
     */
    public function show(College $college)
    {
        // TODO: Gate to Check if User can report for this College
        // Admin, Dean
        $this->authorize('view', $college);

        $departments = $college->departments;

        return view('teachingHonors::report.teaching-honors.college.show', compact(
            'college',
            'departments'
        ));
    }

}
