<?php

namespace Yeltrik\TeachingHonors\app\http\controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Yeltrik\Profile\app\models\Profile;
use Yeltrik\Profile\app\UserProfile;
use Yeltrik\TeachingHonors\app\models\Nomination;
use Yeltrik\TeachingHonors\app\models\Nominator;
use Yeltrik\TeachingHonors\app\models\Nominee;

class UserNominationController extends Controller
{

    /**
     * UserNominationController constructor.
     */
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @param User $user
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function index(User $user)
    {
        // TODO: Only Allow Viewing if This is Auth User, or Auth User is Admin
        $emails = explode(",", env('ADMIN_EMAILS'));
        $email = auth()->user()->email;
        $userIsAdmin = in_array(strtolower($email), $emails);

        if ( $user->id === auth()->user()->id || $userIsAdmin ) {
            $nominations = Nomination::forUser($user);
            if ($nominations !== NULL ) {
                return view('teachingHonors::user.nomination.index', compact([
                    'user',
                    'nominations'
                ]));
            } else {
                abort(403, 'Unable to Load Nominations...');
            }
        } else {
            abort(403, 'Access denied');
        }
    }

}
