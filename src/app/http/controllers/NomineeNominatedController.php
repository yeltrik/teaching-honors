<?php

namespace Yeltrik\TeachingHonors\app\http\controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Yeltrik\Profile\app\models\Profile;
use Yeltrik\TeachingHonors\app\mail\NomineeNominated;

class NomineeNominatedController extends Abstract_NominatedController
{

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $term = $this->term();
        $nominees = $this->nomineesForTermQuery()->get();

        $nomineeEmails = [];
        $nomineesWithoutEmail = [];
        $nomineesWithoutName = [];
        foreach($nominees as $nominee) {
            $profile = $nominee->profile;
            if ( $profile instanceof Profile ) {
                $emails  = $profile->emails;
                if ( $emails->count() > 0 ) {
                    foreach ($emails as $email) {
                        $nomineeEmails[] = $email->email;
                    }
                } else {
                    $nomineesWithoutEmail[] = $nominee;
                }
                $personalNames  = $profile->personalNames;
                if ( $personalNames->count() > 0 ) {

                } else {
                    $nomineesWithoutName[] = $nominee;
                }
            } else {
                $nomineesWithoutEmail[] = $nominee;
            }
        }

        array_unique($nomineeEmails);

        $preview = $this->preview();

        return view('teachingHonors::mail.nominee.nominated.index', compact(
            'term',
            'nominees',
            'nomineeEmails',
            'nomineesWithoutEmail',
            'nomineesWithoutName',
            'preview'
        ));
    }

    /**
     * @return NomineeNominated
     */
    public function preview()
    {
        $this->authorize('viewAny', $this->mailableClassname());

        $nominee = $this->nomineesForTermQuery()->first();
        return new NomineeNominated($nominee);
    }

    /**
     * @return string
     */
    protected function mailableClassname(): string
    {
        return NomineeNominated::class;
    }

    /**
     * @return array
     */
    protected function mailableAttributes(): array
    {
        $attributes = [];
        foreach($this->nomineesForTermQuery()->get() as $nominee) {
            $attributes[] = [
                $nominee
            ];
        }
        return $attributes;
    }
}
