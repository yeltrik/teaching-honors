<?php

namespace Yeltrik\TeachingHonors\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Yeltrik\Profile\app\models\Profile;
use Yeltrik\TeachingHonors\app\mail\DepartmentNomineesNominated;
use Yeltrik\TeachingHonors\app\models\Nomination;
use Yeltrik\TeachingHonors\app\models\Nominee;
use Yeltrik\TeachingHonors\app\NominationWindow;
use Yeltrik\UniTrm\app\models\Term;

abstract class Abstract_NominatedController extends Controller
{
    /**
     * DepartmentNomineesNominatedController constructor.
     */
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    abstract public function index();

    abstract protected function mailableAttributes(): array;

    abstract protected function mailableClassname(): string;

    abstract public function preview();

    /**
     * @return false
     */
    protected function canSendMail()
    {
        return env('MAILABLE') === true;
    }

    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function mail()
    {
        $this->authorize('mail', $this->mailableClassname());

        $mailableClassname = $this->mailableClassname();
        foreach ($this->mailableAttributes() as $mailableAttribute) {
            $mailable = new $mailableClassname(...$mailableAttribute);
            if ($this->canSendMail()) {
                $mailable->initTo();
                $bcc = env('MAILABLE_BCC');
                dd([
                    'from' => $mailable->from,
                    'to' => $mailable->to,
                    'bcc' => $bcc,
                    'subject' => $mailable->subject
                ]);
                Mail::bcc($bcc)
                    ->send($mailable);
            } else {
                $mailable->to(auth()->user()->email);
                $bcc = env('MAILABLE_BCC');
                //dd([
                //    "todo: Enable MAILABLE (env)",
                //    'from' => $mailable->from,
                //    'to' => $mailable->to,
                //    'bcc' => $bcc,
                //    'subject' => $mailable->subject
                //]);

                Mail::bcc($bcc)
                    ->send($mailable);
            }
        }
    }

    /**
     * @return Builder
     */
    protected function nomineesForTermQuery()
    {
        return Nominee::query()
            ->whereIn('id', $this->nominationsForTermQuery()
                ->pluck('nominee_id')
                ->toArray()
            );
    }

    /**
     * @return Builder
     */
    protected function nominationsForTermQuery()
    {
        return Nomination::query()
            ->where('term_id', '=', $this->term()->id);
    }

    /**
     * @return Builder|Model|object|Term|null
     */
    protected function term()
    {
        $term = NominationWindow::openTerm();
        if ($term instanceof Term) {
            return $term;
        } else {
            return NominationWindow::recentlyClosedTerm();
        }
    }

}
