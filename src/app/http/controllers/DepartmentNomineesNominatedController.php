<?php

namespace Yeltrik\TeachingHonors\app\http\controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Yeltrik\ImportProfileAsanaUniMbr\app\models\ProfileUniMbr;
use Yeltrik\TeachingHonors\app\mail\DepartmentNomineesNominated;
use Yeltrik\UniMbr\app\models\DepartmentHead;
use Yeltrik\UniMbr\app\models\Faculty;

class DepartmentNomineesNominatedController extends Abstract_NominatedController
{

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $this->authorize('viewAny', $this->mailableClassname());

        $term = $this->term();
        $departmentHeads = DepartmentHead::query()->get();
        $departmentHeadsWithNominations = $this->departmentHeadsWithNominationsForTerm()->get();
        $departmentHeadEmails = [];
        $departmentHeadsWithoutEmail = [];
        $departmentHeadsWithoutName = [];

        foreach ($departmentHeadsWithNominations as $departmentHead) {
            if ($departmentHead->member instanceof \Yeltrik\UniMbr\app\models\Member) {
                if ($departmentHead->member->email != NULL) {
                    $departmentHeadEmails[] = $departmentHead->member->email;
                } else {
                    $departmentHeadsWithoutEmail[] = $departmentHead;
                }
                if ($departmentHead->member->name == NULL) {
                    $departmentHeadsWithoutName[] = $departmentHead;
                }
            }
        }

        $preview = $this->preview();

        return view('teachingHonors::mail.department.nominee.nominated.index', compact(
            'term',
            'departmentHeads',
            'departmentHeadsWithNominations',
            'departmentHeadEmails',
            'departmentHeadsWithoutEmail',
            'departmentHeadsWithoutName',
            'preview'
        ));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function departmentHeadsWithNominationsForTerm()
    {
        return DepartmentHead::query()
            ->whereIn('department_id', Faculty::query()
                ->whereIn('member_id', ProfileUniMbr::query()
                    ->whereIn('profile_id', $this->nomineesForTermQuery()
                        ->pluck('profile_id')
                        ->toArray()
                    )
                    ->pluck('member_id')
                    ->toArray()
                )
                ->pluck('department_id')
                ->toArray()
            );
    }

    /**
     * @return DepartmentNomineesNominated
     */
    public function preview()
    {
        $this->authorize('viewAny', $this->mailableClassname());

        $departmentHead = $this->departmentHeadsWithNominationsForTerm()->first();
        return new DepartmentNomineesNominated($departmentHead);
    }

    /**
     * @return array
     */
    protected function mailableAttributes(): array
    {
        $attributes = [];
        foreach($this->departmentHeadsWithNominationsForTerm()->get() as $departmentHead) {
            $attributes[] = [
                $departmentHead
            ];
        }
        return $attributes;
    }

    /**
     * @return string
     */
    protected function mailableClassname(): string
    {
        return DepartmentNomineesNominated::class;
    }

}
