<?php

namespace Yeltrik\TeachingHonors\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Yeltrik\TeachingHonors\app\models\Example;
use Yeltrik\TeachingHonors\app\models\Nomination;
use Yeltrik\TeachingHonors\app\models\Nominator;
use Yeltrik\TeachingHonors\app\models\Nominee;

class NomineeNominationController extends Controller
{

    /**
     * ReportPdController constructor.
     */
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @param Nominee $nominee
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Nominee $nominee, Request $request)
    {
        $request->validate([
            'term_id' => 'required',
            'nomination_example' => 'required',
            'nominator_profile_id' => 'required',
            "reason_ids" => 'required|array|min:1',
        ]);

        // Check if a Nomination has already been submitted for Term/Nominee/Nominator
        $termId = $request->term_id;
        $nominatorProfileId = $request->nominator_profile_id;
        $reasonIds = $request->reason_ids;
        $exampleText = $request->nomination_example;

        $nominator = Nominator::query()
            ->where('profile_id', '=', $nominatorProfileId)
            ->first();

        if ($nominator instanceof Nominator === FALSE) {
            $nominator = new Nominator();
            $nominator->profile()->associate($nominatorProfileId);
            $nominator->save();
        }

        if (static::_nominationDoesNotExists($termId, $nominee->id, $nominator->id)) {
            $nomination = new Nomination();
            $nomination->term()->associate($termId);
            $nomination->nominee()->associate($nominee);
            $nomination->nominator()->associate($nominator);

            $example = new Example();
            $example->example = $exampleText;
            $example->save();
            $nomination->example()->associate($example);

            $nomination->save();

            foreach($reasonIds as $reasonId) {
                $nomination->reasons()->attach($reasonId);
            }
            $nomination->save();

            return redirect()->route('teaching-honors.nominations.show', $nomination);
        } else {
            return redirect()->route('teaching-honors.users.nominations.index', auth()->user());
        }
    }

    /**
     * @param int $termId
     * @param int $nomineeId
     * @param int $nominatorId
     * @return bool
     */
    private static function _nominationDoesNotExists(int $termId, int $nomineeId, int $nominatorId)
    {
        return !Nomination::query()
            ->where('term_id', '=', $termId)
            ->where('nominee_id', '=', $nomineeId)
            ->where('nominator_id', '=', $nominatorId)
            ->exists();
    }

}
