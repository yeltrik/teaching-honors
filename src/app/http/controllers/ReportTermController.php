<?php

namespace Yeltrik\TeachingHonors\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Yeltrik\TeachingHonors\app\chart\OutstandingData;
use Yeltrik\UniTrm\app\models\Term;

class ReportTermController extends Controller
{

    /**
     * ReportPdController constructor.
     */
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $terms = Term::query()->get();

        $outstandingData = new OutstandingData();
        $labels = $outstandingData->labels();
        $datasets = $outstandingData->datasets();

        return view('teachingHonors::report.teaching-honors.term.index', compact(
            'terms',
            'labels', 'datasets'
        ));
    }

    /**
     * @param Term $term
     * @return Application|Factory|View
     */
    public function show(Term $term)
    {
        $terms = Term::query()->get();

        $outstandingData = new OutstandingData();
        $outstandingData->forTerm($term);
        $labels = $outstandingData->labels();
        $datasets = $outstandingData->datasets();

        return view('teachingHonors::report.teaching-honors.term.show', compact(
            'terms', 'term',
            'labels', 'datasets'
        ));
    }

}
