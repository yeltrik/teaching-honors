<?php

namespace Yeltrik\TeachingHonors\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Yeltrik\ImportProfileAsanaUniMbr\app\models\ProfileUniMbr;
use Yeltrik\Profile\app\models\Email;
use Yeltrik\Profile\app\models\PersonalName;
use Yeltrik\Profile\app\models\Profile;
use Yeltrik\TeachingHonors\app\models\Nomination;
use Yeltrik\TeachingHonors\app\models\Nominee;
use Yeltrik\UniMbr\app\models\DepartmentHead;
use Yeltrik\UniMbr\app\models\Faculty;
use Yeltrik\UniMbr\app\models\Member;
use Yeltrik\UniOrg\app\models\Department;

class ReportDepartmentController extends Controller
{

    /**
     * ReportPdController constructor.
     */
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @param Department $department
     * @return Application|Factory|View
     */
    public function show(Department $department)
    {
        // TODO: Gate to Check if User can report for this Department
        // Admin, Dean, Department Head
        $this->authorize('view', $department);

        $college = $department->college;

        // Nominees that are faculty associated with this department.
        $nominees = Nominee::query()
            ->whereIn('id', Nomination::query()
                ->pluck('nominee_id')
                ->toArray()
            )
            ->whereIn('profile_id', Profile::query()
                ->whereIn('id', ProfileUniMbr::query()
                    ->whereIn('member_id', Member::query()
                        ->whereIn('id', Faculty::query()
                            ->where('department_id', '=', $department->id)
                            ->pluck('member_id')
                            ->toArray()
                        )
                        ->pluck('id')
                        ->toArray()
                    )
                    ->pluck('profile_id')
                    ->toArray()
                )
                ->pluck('id')
                ->toArray()
            )
            ->get();

        $nomineeNames = [];
        foreach ($nominees as $nominee) {
            $key = "";
            $profile = $nominee->profile;
            if ($profile instanceof Profile) {
                $key = $nominee->id;
                $email = $profile->emails()->first();
                if ($email instanceof Email) {
                    $key = $email->email;
                }
                $personalName = $profile->personalNames()->first();
                if ($personalName instanceof PersonalName) {
                    $key = $personalName->fullName;
                }
            } else {
                $key = $nominee->id;
            }
            $nomineeNames[] = $key;
        }

        $nominations = Nomination::query()
            ->orderBy('created_at', 'desc')
            ->whereIn('nominee_id', Nominee::query()
                ->whereIn('profile_id', Profile::query()
                    ->whereIn('id', ProfileUniMbr::query()
                        ->whereIn('member_id', Member::query()
                            ->whereIn('id', Faculty::query()
                                ->where('department_id', '=', $department->id)
                                ->pluck('member_id')
                                ->toArray()
                            )
                            ->pluck('id')
                            ->toArray()
                        )
                        ->pluck('profile_id')
                        ->toArray()
                    )
                    ->pluck('id')
                    ->toArray()
                )
                ->pluck('id')
                ->toArray()
            )
            ->get();

        return view('teachingHonors::report.teaching-honors.department.show', compact(
            'college',
            'department',
            'nomineeNames',
            'nominations'
        ));
    }

}
