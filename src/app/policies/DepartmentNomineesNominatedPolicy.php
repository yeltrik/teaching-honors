<?php

namespace Yeltrik\TeachingHonors\app\policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Gate;

class DepartmentNomineesNominatedPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return (Gate::forUser($user)->allows('batchMail'));
    }

    /**
     * @param User $user
     * @return bool
     */
    public function mail(User $user)
    {
        return (Gate::forUser($user)->allows('batchMail'));
    }

}
