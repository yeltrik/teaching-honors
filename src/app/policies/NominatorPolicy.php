<?php

namespace Yeltrik\TeachingHonors\app\policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Yeltrik\Profile\app\UserProfile;
use Yeltrik\TeachingHonors\app\models\Nominator;
use Yeltrik\TeachingHonors\app\providers\ServiceProvider;

class NominatorPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public
    function viewAny(User $user)
    {
        if ( $user->can('report')) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Nominator $nominator
     * @return mixed
     */
    public
    function view(User $user, Nominator $nominator)
    {
        if ( $user->can('report')) {
            return TRUE;
        }

        $userProfile = new UserProfile($user);
        if ($nominator->profile->id === $userProfile->profile()->id) {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * @param User $user
     * @param Nominator $nominator
     */
    public function destroy(User $user, Nominator $nominator)
    {
        $emails = explode(",", env(ServiceProvider::ADMIN_EMAILS_ENV_KEY));
        if (empty($emails)) {
            dd('Need to Set ' . ServiceProvider::ADMIN_EMAILS_ENV_KEY . ' Addresses in .env');
        } else {
            $email = $user->email;
            return in_array(strtolower($email), $emails);
        }
        return FALSE;
    }

    public function transferAny(User $user) {
        $emails = explode(",", env(ServiceProvider::ADMIN_EMAILS_ENV_KEY));
        if (empty($emails)) {
            dd('Need to Set ' . ServiceProvider::ADMIN_EMAILS_ENV_KEY . ' Addresses in .env');
        } else {
            $email = $user->email;
            return in_array(strtolower($email), $emails);
        }
       return FALSE;
    }

}
