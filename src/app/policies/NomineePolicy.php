<?php

namespace Yeltrik\TeachingHonors\app\policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Yeltrik\Profile\app\UserProfile;
use Yeltrik\TeachingHonors\app\models\Nominee;
use Yeltrik\TeachingHonors\app\providers\ServiceProvider;

class NomineePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public
    function viewAny(User $user)
    {
        if ( $user->can('report')) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Nominee $nominee
     * @return mixed
     */
    public
    function view(User $user, Nominee $nominee)
    {
        if ( $user->can('report')) {
            return TRUE;
        }

        $userProfile = new UserProfile($user);
        if ($nominee->profile->id === $userProfile->profile()->id) {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * @param User $user
     * @param Nominee $nominee
     */
    public function destroy(User $user, Nominee $nominee)
    {
        $emails = explode(",", env(ServiceProvider::ADMIN_EMAILS_ENV_KEY));
        if (empty($emails)) {
            dd('Need to Set ' . ServiceProvider::ADMIN_EMAILS_ENV_KEY . ' Addresses in .env');
        } else {
            $email = $user->email;
            return in_array(strtolower($email), $emails);
        }
        return FALSE;
    }

    public function transferAny(User $user) {
        $emails = explode(",", env(ServiceProvider::ADMIN_EMAILS_ENV_KEY));
        if (empty($emails)) {
            dd('Need to Set ' . ServiceProvider::ADMIN_EMAILS_ENV_KEY . ' Addresses in .env');
        } else {
            $email = $user->email;
            return in_array(strtolower($email), $emails);
        }
       return FALSE;
    }

}
