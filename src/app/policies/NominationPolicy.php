<?php

namespace Yeltrik\TeachingHonors\app\policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Yeltrik\Profile\app\UserProfile;
use Yeltrik\TeachingHonors\app\models\Nomination;
use Yeltrik\TeachingHonors\app\NominationWindow;

class NominationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Nomination $nomination
     * @return mixed
     */
    public function view(User $user, Nomination $nomination)
    {
        $userProfile = new UserProfile($user);
        return (
            $nomination->nominator->profile->id === $userProfile->profile()->id ||
            $nomination->nominee->profile->id === $userProfile->profile()->id
        );
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return (
            NominationWindow::isOpen() ||
            $user->can('nominate')
        );
    }

}
