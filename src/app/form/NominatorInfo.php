<?php


namespace Yeltrik\TeachingHonors\app\form;


use Illuminate\Database\Eloquent\HigherOrderBuilderProxy;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Auth;
use Yeltrik\Profile\app\models\PersonalName;
use Yeltrik\Profile\app\models\Profile;
use Yeltrik\Profile\app\UserProfile;

class NominatorInfo
{

    /**
     * @return HigherOrderBuilderProxy|mixed|null
     */
    public function email()
    {
        $profile = $this->profile();
        if ($profile instanceof Profile) {
            $emails = $profile->emails();
            if ($emails->count() > 0) {
                return $emails->first()->email;
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    }

    /**
     * @return HigherOrderBuilderProxy|mixed|string|null
     */
    public function firstName()
    {
        $personalName = $this->personalName();
        if ( $personalName instanceof PersonalName ) {
            return $personalName->first;
        } else {
            return NULL;
        }
    }

    /**
     * @return HigherOrderBuilderProxy|mixed|string|null
     */
    public function lastName()
    {
        $personalName = $this->personalName();
        if ( $personalName instanceof PersonalName ) {
            return $personalName->last;
        } else {
            return NULL;
        }
    }

    /**
     * @return Model|HasMany|object|null
     */
    public function personalName()
    {
        $profile = $this->profile();
        if ($profile instanceof Profile) {
            $personalNames = $profile->personalNames();
            if ( $personalNames->count() > 0) {
                return $personalNames->first();
            }
        }
        return NULL;
    }

    /**
     * @param bool $create
     * @return Profile|null
     */
    public function profile(bool $create = TRUE)
    {
        return (new UserProfile(Auth::user()))->profile($create);
    }

}
