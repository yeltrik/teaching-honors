<?php

namespace Yeltrik\TeachingHonors\app\form;

use Illuminate\Database\Eloquent\Builder;
use Iterator;
use Yeltrik\ImportProfileAsanaUniMbr\app\models\ProfileUniMbr;
use Yeltrik\Profile\app\models\Profile;
use Yeltrik\TeachingHonors\app\models\Nominee;
use Yeltrik\UniMbr\app\models\Faculty;
use Yeltrik\UniMbr\app\models\Member;

class NomineeSelectOptions implements Iterator
{

    private array $array;
    private bool $facultyOnly = TRUE;
    private int $index = 0;
    private bool $membersOnly = TRUE;


    /**
     * NomineeSelectOptions constructor.
     */
    public function __construct()
    {
        $this->array = $this->optionsArray();
    }

    /**
     * @return mixed
     */
    public function current()
    {
        return $this->array[$this->key()];
    }

    /**
     * @return bool
     */
    public function empty()
    {
        return empty($this->array);
    }

    /**
     * @param bool $value
     */
    public function facultyOnly(bool $value = TRUE): void
    {
        $this->facultyOnly = $value;
        $this->array = $this->optionsArray();
        $this->rewind();
    }

    /**
     * @return bool|float|int|string|null
     */
    public function key()
    {
        $arrayKeys = array_keys($this->array);
        if (isset($arrayKeys[$this->index])) {
            return $arrayKeys[$this->index];
        } else {
            return NULL;
        }
    }

    /**
     *
     */
    public function next()
    {
        $this->index++;
    }

    /**
     * @param bool $value
     */
    public function membersOnly(bool $value = TRUE)
    {
        $this->membersOnly = $value;
        $this->array = $this->optionsArray();
        $this->rewind();
    }

    /**
     * @return Builder
     */
    private function memberQuery()
    {
        $query = Member::query();

        if ($this->facultyOnly) {
            $query->whereIn('id', Faculty::query()
                ->pluck('member_id')
                ->toArray()
            );
        }

        return $query;
    }

    /**
     * @return Builder
     */
    private function nomineeProfiles()
    {
        $query = Profile::query();
        if ($this->membersOnly) {
            $query->whereIn('id', $this->profileUniMbrQuery()
                ->pluck('profile_id')
                ->toArray()
            );
        }
        return $query;
    }

    /**
     * @return array
     */
    public function optionsArray(): array
    {
        $nomineeSelectOptions = [];

        foreach (static::nomineeProfiles()->get() as $nomineeProfile) {
            $personalNames = $nomineeProfile->personalNames;
            $nominee = Nominee::query()
                ->where('profile_id', '=', $nomineeProfile->id)
                ->first();
            if ( $nominee instanceof Nominee ) {
                $key = $nominee->id;
                if ($personalNames->count() > 0) {
                    $nomineeSelectOptions[$key] = $personalNames->first()->lastFirst;
                } else {
                    $emails = $nomineeProfile->emails;
                    if ($emails->count() > 0) {
                        $nomineeSelectOptions[$key] = $emails->first()->email;
                    }
                }
            }
        }
        asort($nomineeSelectOptions);
        return $nomineeSelectOptions;
    }

    /**
     * @return Builder
     */
    private function profileUniMbrQuery()
    {
        $query = ProfileUniMbr::query();
        if ($this->membersOnly) {
            $query->whereIn('member_id', $this->memberQuery()
                ->pluck('id')
                ->toArray()
            );
        }

        return $query;
    }

    /**
     *
     */
    public function rewind()
    {
        $this->index = 0;
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return isset($this->array[$this->key()]);
    }

}
