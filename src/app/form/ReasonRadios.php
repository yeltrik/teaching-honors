<?php


namespace Yeltrik\TeachingHonors\app\form;


use Illuminate\Database\Eloquent\Builder;
use Iterator;
use Yeltrik\TeachingHonors\app\models\Reason;

class ReasonRadios implements Iterator
{

    private bool $activeOnly = TRUE;
    private array $array;
    private int $index = 0;
    public static string $reasonValuePrefix = "reason_";

    /**
     * ReasonRadios constructor.
     */
    public function __construct()
    {
        $this->array = $this->reasonsArray();
    }

    /**
     * @return mixed
     */
    public function current()
    {
        return $this->array[$this->key()];
    }

    /**
     * @return bool|float|int|string|null
     */
    public function key()
    {
        $arrayKeys = array_keys($this->array);
        if (isset($arrayKeys[$this->index])) {
            return $arrayKeys[$this->index];
        } else {
            return NULL;
        }
    }

    /**
     *
     */
    public function next()
    {
        $this->index++;
    }

    /**
     * @return array
     */
    private function reasonsArray(): array
    {
        $array = [];

        foreach ($this->reasonQuery()->get() as $reason) {
            $key = $reason->id;
            $array[$key] = $reason->text;
        }

        return $array;
    }

    /**
     * @return Builder
     */
    private function reasonQuery()
    {
        $query = Reason::query();

        if ($this->activeOnly) {
            $query->where('active', '=', TRUE);
        }

        return $query;
    }

    /**
     *
     */
    public function rewind()
    {
        $this->index = 0;
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return isset($this->array[$this->key()]);
    }

}
