<?php

namespace Yeltrik\TeachingHonors\app;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Yeltrik\UniTrm\app\models\Term;
use Yeltrik\UniTrm\app\models\Year;

class NominationWindow
{

    public static array $termWindows = [
        'Sp 19' => [
            'open' => '2019-04-01',
            'close' => '2019-05-31'
        ],
        'Fa 19' => [
            'open' => '2019-11-01',
            'close' => '2019-12-31'
        ],
        'Sp 20' => [
            'open' => '2020-05-01',
            'close' => '2020-05-31'
        ],
        'Fa 20' => [
            'open' => '2020-11-30',
            'close' => '2020-12-05'
        ],
        'Sp 21' => [
            'open' => '2021-04-12',
            'close' => '2021-04-23'
        ]
    ];

    /**
     * @return bool
     */
    public static function hasPassed(): bool
    {
        if (static::isOpen()) {
            return FALSE;
        } else {
            foreach (static::$termWindows as $termWindow) {
                $open = strtotime($termWindow['open']);
                $now = strtotime('now');
                if ($open > $now) {
                    return FALSE;
                }
            }
        }
        return TRUE;
    }

    /**
     * @return bool
     */
    public static function isOpeningSoon(): bool
    {
        return (
            !static::isOpen() &&
            static::nextOpen() != NULL
        );
    }

    /**
     * @return bool
     */
    public static function isOpen(): bool
    {
        $now = strtotime('now');
        foreach (static::$termWindows as $termWindow) {
            $open = strtotime($termWindow['open']);
            $close = strtotime($termWindow['close']);
            if ($open <= $now && $close >= $now) {
                return TRUE;
            }
        }
        return false;
    }

    /**
     * @return string
     */
    public static function nextClose()
    {
        $now = strtotime('now');
        foreach (static::$termWindows as $key => $termWindow) {
            $close = strtotime($termWindow['close']);
            if ($close > $now) {
                return $termWindow['close'];
            }
        }
        return NULL;
    }

    /**
     * @return string
     */
    public static function nextOpen()
    {
        $now = strtotime('now');
        foreach (static::$termWindows as $key => $termWindow) {
            $open = strtotime($termWindow['open']);
            if ($open > $now) {
                return $termWindow['open'];
            }
        }
        return NULL;
    }

    /**
     * @return Builder|Model|object|Term|null
     */
    public static function openTerm()
    {
        if (static::isOpen()) {
            $now = strtotime('now');
            foreach (static::$termWindows as $key => $termWindow) {
                $open = strtotime($termWindow['open']);
                $close = strtotime($termWindow['close']);
                if ($open <= $now && $close >= $now) {
                    return static::termFromKey($key);
                }
            }
        } else {
            return NULL;
        }
    }

    /**
     * @return Builder|Model|object|Term|null
     */
    public static function recentlyClosedTerm()
    {
        $closedTerm = NULL;
        $now = strtotime('now');
        $highestClosed = "";
        foreach (static::$termWindows as $key => $termWindow) {
            $open = strtotime($termWindow['open']);
            $close = strtotime($termWindow['close']);
            if ( $close < $now ) {
                if ($highestClosed == NULL ) {
                    $closedTerm = static::termFromKey($key);
                } else if ( $highestClosed < $close) {
                    $closedTerm = static::termFromKey($key);
                }
            }
        }

        return $closedTerm;
    }

    private static function termFromKey(string $key)
    {
        $parts = explode(" ", $key);
        $termStr = $parts[0];
        $yearInt = $parts[1];

        $year = Year::query()
            ->where('abbr', '=', $yearInt)
            ->first();

        if ( $year instanceof Year ) {
            $terms = Term::query()
                ->where('abbr', '=', $termStr)
                ->where('year_id', '=', $year->id);
            if ( $terms->exists() ) {
                return $terms->first();
            } else {
                $term = new Term();
                $term->year()->associate($year);
                $term->name = $termStr;
                $term->abbr = $termStr;
                $term->save();

                return $term;
            }
        } else {
            $year = new Year();
            $year->name = $yearInt;
            $year->abbr = $yearInt;
            $year->save();

            $term = new Term();
            $term->year()->associate($year);
            $term->name = $termStr;
            $term->abbr = $termStr;
            $term->save();

            return $term;
        }
    }

}
