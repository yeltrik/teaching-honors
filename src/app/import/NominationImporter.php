<?php

namespace Yeltrik\TeachingHonors\app\import;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yeltrik\TeachingHonors\app\import\decoder\NominatorDecoder;
use Yeltrik\TeachingHonors\app\import\decoder\NomineeDecoder;
use Yeltrik\TeachingHonors\app\import\decoder\ReasonDecoder;
use Yeltrik\TeachingHonors\app\import\decoder\TermDecoder;
use Yeltrik\TeachingHonors\app\models\Example;
use Yeltrik\TeachingHonors\app\models\Nomination;
use Yeltrik\TeachingHonors\app\models\NominationReason;
use Yeltrik\TeachingHonors\app\models\Nominator;
use Yeltrik\TeachingHonors\app\models\Nominee;
use Yeltrik\TeachingHonors\app\models\Reason;
use Yeltrik\UniTrm\app\models\Term;

class NominationImporter
{

    const FILE_PROPERTY_NAME = "json_file";

    private ?string $jsonPath = null;

    private Request $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param string $path
     */
    public function useJson(string $path)
    {
        $this->jsonPath = $path;
    }

    /**
     * @param array $jsonNomination
     * @param Term $term
     * @param Nominee $nominee
     * @param Nominator $nominator
     * @return Nomination
     */
    private function newFromJsonNomination(array $jsonNomination, Term $term, Nominee $nominee, Nominator $nominator)
    {
        $nomination = new Nomination();
        $nomination->term()->associate($term);
        $nomination->nominee()->associate($nominee);
        $nomination->nominator()->associate($nominator);
        $example = new Example();
        $exampleStr = $jsonNomination['nomination_reasons'];
        $example->example = $exampleStr;
        $example->save();
        $nomination->example()->associate($example);
        $createdAt = $jsonNomination['created_at'];
        $nomination->created_at = $createdAt;
        $nomination->save();
        return $nomination;
    }

    /**
     * @return RedirectResponse
     * @throws FileNotFoundException
     */
    public function process()
    {
        foreach ($this->json($this->request) as $jsonNomination) {
            // Get Term Based on Date Range
            $term = TermDecoder::fromJsonNomination($jsonNomination);
            // Load a Nominator
            $nominator = NominatorDecoder::fromJsonNomination($jsonNomination);
            // Load a Nominee
            $nominee = NomineeDecoder::fromJsonNomination($jsonNomination);
            // Check for a Nomination by Nominator and Nominee for Term
            $nomination = Nomination::query()
                ->where('term_id', '=', $term->id)
                ->where('nominee_id', '=', $nominee->id)
                ->where('nominator_id', '=', $nominator->id)
                ->first();
            if ($nomination instanceof Nomination === FALSE) {
                $nomination = static::newFromJsonNomination($jsonNomination, $term, $nominee, $nominator);
            }
            ReasonDecoder::fromJsonNomination($jsonNomination, $nomination);
        }

        //dd([
        //    'Nomination Importer',
        //    'nominators' => Nominator::query()->count(),
        //    'nominees' => Nominee::query()->count(),
        //    'nominations' => Nomination::query()->count(),
        //    'examples' => Example::query()->count(),
        //    'nominationReasons' => NominationReason::query()->count(),
        //    'reasons' => Reason::query()->count(),
        //    'terms' => Term::query()->count()
        //]);

        return back()
            ->with('success', 'You have successfully upload file.');
    }

    /**
     * @param Request $request
     */
    public static function validate(Request $request)
    {
        if ($request->import_method == "export_json") {
            $request->validate([
                'json_file' => 'required|mimes:txt|max:204800',
            ]);
        }
    }

    /**
     * @param Request $request
     * @return array
     * @throws FileNotFoundException
     */
    public function json(Request $request)
    {
        $propertyName = static::FILE_PROPERTY_NAME;
        $fileName = time() . '_' . $request->$propertyName->getClientOriginalName();
        $filePath = $request->file(static::FILE_PROPERTY_NAME)->storeAs('uploads', $fileName, 'public');

        return (array)json_decode(Storage::disk('public')->get($filePath), true);
    }

}
