<?php


namespace Yeltrik\TeachingHonors\app\import\decoder;


use Yeltrik\TeachingHonors\app\models\Nomination;
use Yeltrik\TeachingHonors\app\models\Reason;

class ReasonDecoder
{

    /**
     * @param array $jsonNomination
     * @param Nomination $nomination
     */
    public static function fromJsonNomination(array $jsonNomination, Nomination $nomination)
    {
        $nomineeAttributes = $jsonNomination['nominee_attributes'];
        foreach ($nomineeAttributes as $nomineeAttribute) {
            $reason = static::fromNomineeAttribute($nomineeAttribute, TRUE);
            if ( $nomination->reasons->contains('id', $reason->id) === FALSE) {
                $nomination->reasons()->attach($reason);
            }
            $nomination->save();
        }
    }

    /**
     * @param $nomineeAttribute
     * @param bool $create
     */
    private static function fromNomineeAttribute(string $nomineeAttribute, bool $create)
    {
        $reason = Reason::query()
            ->where('text', '=', $nomineeAttribute)
            ->first();
        if ($reason instanceof Reason) {
            return $reason;
        } elseif ($create === TRUE) {
            $reason = new Reason();
            $reason->text = $nomineeAttribute;
            $reason->save();
            return $reason;
        } else {
            return NULL;
        }
    }

}
