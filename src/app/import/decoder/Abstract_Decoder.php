<?php


namespace Yeltrik\TeachingHonors\app\import\decoder;


use Illuminate\Database\Eloquent\HigherOrderBuilderProxy;
use Yeltrik\Profile\app\models\Email;
use Yeltrik\Profile\app\models\PersonalName;
use Yeltrik\Profile\app\models\Profile;

abstract class Abstract_Decoder
{

    /**
     * @param string $emailStr
     * @return HigherOrderBuilderProxy|mixed|Profile|null
     */
    protected static function profileFromEmail(string $emailStr)
    {
        $email = Email::query()
            ->where('email', '=', $emailStr)
            ->first();
        if ($email instanceof Email) {
            return $email->profile;
        } else {
            return NULL;
        }
    }

    /**
     * @param string $firstName
     * @param string $lastName
     * @return HigherOrderBuilderProxy|mixed|Profile|null
     */
    protected static function profileFromNames(string $firstName, string $lastName)
    {
        $personalName = PersonalName::query()
            ->where('first', '=', $firstName)
            ->where('last', '=', $lastName)
            ->first();
        if ($personalName instanceof PersonalName) {
            return $personalName->profile;
        } else {
            return NULL;
        }
    }

}
