<?php


namespace Yeltrik\TeachingHonors\app\import\decoder;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Yeltrik\Profile\app\models\PersonalName;
use Yeltrik\Profile\app\models\Profile;
use Yeltrik\TeachingHonors\app\models\Nominee;

class NomineeDecoder extends Abstract_Decoder
{

    /**
     * @param array $jsonNomination
     * @return Nominee
     */
    public static function fromJsonNomination(array $jsonNomination)
    {
        $firstName = $jsonNomination["nominee_first_name"];
        $lastName = $jsonNomination["nominee_last_name"];
        //$department = $jsonNomination["nominee_department"];
        //$college = $jsonNomination["nominee_college"];

        // Check for Nominee where Profile has Personal Name that matches
        $nominee = static::nomineeFromNames($firstName, $lastName);
        // If no Match, then Create Nominee, Profile, Email, Personal Name
        if ($nominee instanceof Nominee === FALSE) {
            $nominee = new Nominee();

            $profile = static::profileFromNames($firstName, $lastName);
            if ($profile instanceof Profile === FALSE) {
                $profile = new Profile();
                $profile->save();
            }
            $nominee->profile()->associate($profile);
            $nominee->save();

            if ($profile->personalNames()->count() === 0) {
                $personalName = new PersonalName();
                $personalName->profile()->associate($profile);
                $personalName->first = $firstName;
                $personalName->last = $lastName;
                $personalName->save();
            }

        }
        // ? Set College / Department for Nominee??
        // ? Staff or Faculty in Uni Membership>
        return $nominee;
    }

    /**
     * @param string $firstName
     * @param string $lastName
     * @return Builder|Model|object|null
     */
    private static function nomineeFromNames(string $firstName, string $lastName)
    {
        $profile = static::profileFromNames($firstName, $lastName);
        if ($profile instanceof Profile) {
            return Nominee::query()
                ->where('profile_id', '=', $profile->id)
                ->first();
        } else {
            return NULL;
        }
    }

}
