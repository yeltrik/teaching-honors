<?php


namespace Yeltrik\TeachingHonors\app\import\decoder;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Yeltrik\Profile\app\models\Email;
use Yeltrik\Profile\app\models\PersonalName;
use Yeltrik\Profile\app\models\Profile;
use Yeltrik\TeachingHonors\app\models\Nominator;

class NominatorDecoder extends Abstract_Decoder
{
    /**
     * @param array $jsonNomination
     * @return Nominator
     */
    public static function fromJsonNomination(array $jsonNomination)
    {
        $firstName = $jsonNomination["nominator_first_name"];
        $lastName = $jsonNomination["nominator_last_name"];
        $emailStr = $jsonNomination["nominator_email"];
        //$relationship = $jsonNomination["nominator_relationship"];

        // Check for a Nominator where Profile has Email that matches
        $nominator = static::nominatorFromEmail($emailStr);
        // If no Match, Then check for Nominator where Profile has Personal Name that matches
        if ($nominator instanceof Nominator === FALSE) {
            $nominator = static::nominatorFromNames($firstName, $lastName);
        }
        // If no Match, then Create Nominator, Profile, Email, Personal Name
        if ($nominator instanceof Nominator === FALSE) {
            $nominator = new Nominator();

            // There might already be an Email / Personal Name but not associated with a Nominator
            $profile = static::profileFromEmail($emailStr);
            if ($profile instanceof Profile === FALSE) {
                $profile = new Profile();
                $profile->save();
            }

            $nominator->profile()->associate($profile);
            $nominator->save();

            if ($profile->emails()->count() === 0) {
                $email = new Email();
                $email->profile()->associate($profile);
                $email->email = $emailStr;
                $email->save();
            }

            $personalName = PersonalName::query()
                ->where('first', '=', $firstName)
                ->where('last', '=', $lastName)
                ->first();
            if ($personalName instanceof PersonalName === FALSE) {
                $personalName = new PersonalName();
                $personalName->profile()->associate($profile);
                $personalName->first = $firstName;
                $personalName->last = $lastName;
                $personalName->save();
            }
        }

        return $nominator;
    }

    /**
     * @param string $emailStr
     * @return Builder|Model|object|null
     */
    private static function nominatorFromEmail(string $emailStr)
    {
        $profile = static::profileFromEmail($emailStr);
        if ($profile instanceof Profile) {
            return Nominator::query()
                ->where('profile_id', '=', $profile->id)
                ->first();
        } else {
            return NULL;
        }
    }

    /**
     * @param string $firstName
     * @param string $lastName
     * @return Builder|Model|object|null
     */
    private static function nominatorFromNames(string $firstName, string $lastName)
    {
        $profile = static::profileFromNames($firstName, $lastName);
        if ($profile instanceof Profile) {
            return Nominator::query()
                ->where('profile_id', '=', $profile->id)
                ->first();
        } else {
            return NULL;
        }
    }

}
