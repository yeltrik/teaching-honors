<?php


namespace Yeltrik\TeachingHonors\app\import\decoder;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Yeltrik\TeachingHonors\app\NominationWindow;
use Yeltrik\UniTrm\app\models\Term;
use Yeltrik\UniTrm\app\models\Year;

/**
 * Class TermDecoder
 *
 * Terms windows should represent when the Nomination Submissions have been opened.
 *
 * @package Yeltrik\TeachingHonors\app\import\decoder
 */
class TermDecoder
{

    /**
     * @param array $jsonNomination
     * @return Builder|Model|object|Term|null
     */
    public static function fromJsonNomination(array $jsonNomination)
    {
        $windowIndex = static::windowIndex($jsonNomination);
        return static::termFromWindowIndex($windowIndex, TRUE);
    }

    /**
     * @param string $windowIndex
     * @param bool $create
     * @return Builder|Model|object|Term|null
     */
    private static function termFromWindowIndex(string $windowIndex, bool $create)
    {
        $parts = explode(' ', $windowIndex);
        $abbr = $parts[0];
        $year = static::yearFromWindowIndex($windowIndex, $create);
        if ($year instanceof Year) {
            $term = Term::query()
                ->where('abbr', '=', $abbr)
                ->where('year_id', '=', $year->id)
                ->first();
            if ($term instanceof Term) {
                return $term;
            } else if ($create === TRUE) {
                $term = new Term();
                $term->year()->associate($year);
                $term->name = $abbr;
                $term->abbr = $abbr;
                $term->save();
                return $term;
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    }

    /**
     * @param array $jsonNomination
     * @return int|string|null
     */
    private static function windowIndex(array $jsonNomination)
    {
        foreach (NominationWindow::$termWindows as $key => $subArray) {
            if (static::withinTermWindowSubArray($subArray, $jsonNomination)) {
                return $key;
            }
        }
        return NULL;
    }

    /**
     * @param array $subArray
     * @param array $jsonNomination
     * @return bool
     */
    private static function withinTermWindowSubArray(array $subArray, array $jsonNomination)
    {
        $createdAt = $jsonNomination['created_at'];
        $open = $subArray['open'];
        $close = $subArray['close'];
        if ($createdAt >= $open && $createdAt <= $close) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * @param string $windowIndex
     * @param bool $create
     * @return Builder|Model|object|Year|null
     */
    private static function yearFromWindowIndex(string $windowIndex, bool $create)
    {
        $parts = explode(' ', $windowIndex);
        $abbr = $parts[1];
        $year = Year::query()
            ->where('abbr', '=', $abbr)
            ->first();
        if ($year instanceof Year) {
            return $year;
        } elseif ($create === TRUE) {
            $year = new Year();
            $year->name = $abbr;
            $year->abbr = $abbr;
            $year->save();
            return $year;
        } else {
            return NULL;
        }
    }
}
