<?php

namespace Yeltrik\TeachingHonors\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class NominationReason
 *
 * @property Nomination nomination
 * @property Reason reason
 *
 * @package Yeltrik\TeachingHonors\app\models
 */
class NominationReason extends Model
{
    use HasFactory;

    protected $connection = 'teaching_honors';
    public $table = 'nomination_reason';

    /**
     * @return BelongsTo
     */
    public function nomination()
    {
        return $this->belongsTo(Nomination::class);
    }

    /**
     * @return BelongsTo
     */
    public function reason()
    {
        return $this->belongsTo(Reason::class);
    }

}
