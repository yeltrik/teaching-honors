<?php

namespace Yeltrik\TeachingHonors\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Yeltrik\ImportProfileAsanaUniMbr\app\models\ProfileUniMbr;
use Yeltrik\Profile\app\models\Profile;
use Yeltrik\UniMbr\app\models\Member;

/**
 * Class Nominee
 *
 * @property Collection nominations
 * @property Profile profile
 *
 * @package Yeltrik\TeachingHonors\app\models
 */
class Nominee extends Model
{
    use HasFactory;

    protected $connection = 'teaching_honors';
    public $table = 'nominees';

    /**
     * @return HasMany
     */
    public function nominations()
    {
        return $this->hasMany(Nomination::class);
    }

    public function member()
    {
        return Member::query()
            ->whereIn('id', ProfileUniMbr::query()
                ->where('profile_id', '=', $this->profile->id)
                ->pluck('member_id')
                ->toArray()
            )
            ->first();
    }

    /**
     * @return BelongsTo
     */
    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }

}
