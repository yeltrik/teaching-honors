<?php

namespace Yeltrik\TeachingHonors\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Reason
 *
 * @property string text
 * @property bool active
 *
 * @package Yeltrik\TeachingHonors\app\models
 */
class Reason extends Model
{
    use HasFactory;

    protected $connection = 'teaching_honors';
    public $table = 'reasons';

}
