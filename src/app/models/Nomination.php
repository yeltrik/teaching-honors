<?php

namespace Yeltrik\TeachingHonors\app\models;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Yeltrik\Profile\app\models\Email;
use Yeltrik\Profile\app\models\PersonalName;
use Yeltrik\Profile\app\models\Profile;
use Yeltrik\Profile\app\UserProfile;
use Yeltrik\UniTrm\app\models\Term;

/**
 * Class Nomination
 *
 * @property Example example
 * @property Nominator nominator
 * @property Nominee nominee
 * @property Collection reasons
 * @property Term term
 *
 * @package Yeltrik\TeachingHonors\app\models
 */
class Nomination extends Model
{
    use HasFactory;

    protected $connection = 'teaching_honors';
    public $table = 'nominations';

    /**
     * @return BelongsTo
     */
    public function example()
    {
        return $this->belongsTo(Example::class);
    }

    /**
     * @param User $user
     * @return Builder[]|Collection|null
     */
    public static function forUser(User $user)
    {
        $nominationQuery = Nomination::query();

        // TODO: If Mapping of Multiple Profiles to one Member,
        // then should also include related Profiles through existing uni membership mappings

        $userProfile = new UserProfile($user);
        $profile = $userProfile->profile(TRUE);
        if ($profile instanceof Profile) {
            $nominee = Nominee::query()
                ->where('profile_id', '=', $profile->id)
                ->first();
            if ($nominee instanceof Nominee) {
                $nominationQuery->orWhere('nominee_id', '=', $nominee->id);
            }
            $nominator = Nominator::query()
                ->where('profile_id', '=', $profile->id)
                ->first();
            if ($nominator instanceof Nominator) {
                $nominationQuery->orWhere('nominator_id', '=', $nominator->id);
            }

            if ( !empty($nominationQuery->getQuery()->wheres) ) {
                return $nominationQuery->get();
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    }

    /**
     * @return string
     */
    public function getNominatorIdentificationAttribute()
    {
        $profile = $this->nominator->profile;
        $userProfile = new UserProfile(auth()->user());
        if ( $userProfile->profile()->id === $profile->id ) {
            $personalName = $profile->personalNames->first();
            if ($personalName instanceof PersonalName) {
                return $personalName->fullName;
            }
            $email = $profile->emails->first();
            if ($email instanceof Email) {
                return $email->email;
            }
        } else {
            return "Anonymous";
        }
    }

    /**
     * @return string
     */
    public function getNomineeIdentificationAttribute()
    {
        if ( $this->nominee instanceof Nominee ) {
            $profile = $this->nominee->profile;
            $personalName = $profile->personalNames->first();
            if ($personalName instanceof PersonalName) {
                return $personalName->fullName;
            }
            $email = $profile->emails->first();
            if ($email instanceof Email) {
                return $email->email;
            }
        }
        return "";
    }

    /**
     * @return BelongsTo
     */
    public function nominator()
    {
        return $this->belongsTo(Nominator::class);
    }

    /**
     * @return BelongsTo
     */
    public function nominee()
    {
        return $this->belongsTo(Nominee::class);
    }

    /**
     * @return BelongsToMany
     */
    public function reasons()
    {
        return $this->belongsToMany(Reason::class);
    }

    /**
     * @return BelongsTo
     */
    public function term()
    {
        return $this->belongsTo(Term::class);
    }

}
