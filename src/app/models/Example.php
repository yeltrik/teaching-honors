<?php

namespace Yeltrik\TeachingHonors\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Example
 *
 * @property Nomination nomination
 *
 * @package Yeltrik\TeachingHonors\app\models
 */
class Example extends Model
{
    use HasFactory;

    protected $connection = 'teaching_honors';
    public $table = 'examples';


    public function getAnonymousExampleAttribute()
    {
        // Get Faculty First / Last Name
        $parts = [];
        $parts[] = $this->nomination->nominee->profile->personalNames->first()->first;
        $parts[] = $this->nomination->nominee->profile->personalNames->first()->last;

        $anonymizedExample = $this->example;
        $anonymizedExample = str_replace($parts, "********", $anonymizedExample);

        return $anonymizedExample;
    }

    /**
     * @return HasOne
     */
    public function nomination()
    {
        return $this->hasOne(Nomination::class);
    }

}
