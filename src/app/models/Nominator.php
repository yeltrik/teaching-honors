<?php

namespace Yeltrik\TeachingHonors\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Yeltrik\Profile\app\models\Profile;
use Yeltrik\UniTrm\app\models\Term;

/**
 * Class Nominator
 *
 * @property Profile profile
 *
 * @package Yeltrik\TeachingHonors\app\models
 */
class Nominator extends Model
{
    use HasFactory;

    protected $connection = 'teaching_honors';
    public $table = 'nominators';

    /**
     * @return HasMany
     */
    public function nominations()
    {
        return $this->hasMany(Nomination::class);
    }

    public function nomnationTerms()
    {
        return Term::query()
            ->whereIn('id', $this->nominations()
                ->pluck('term_id')
                ->toArray()
            )
            ->get();
    }

    /**
     * @return BelongsTo
     */
    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }

}
