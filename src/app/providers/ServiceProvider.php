<?php

namespace Yeltrik\TeachingHonors\app\providers;

use Illuminate\Support\Facades\Gate;
use Yeltrik\TeachingHonors\app\NominationWindow;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    const ADMIN_EMAILS_ENV_KEY = "ADMIN_EMAILS";
    const MAILER_ADMINS_EMAILS_ENV_KEY = "MAILER_ADMIN_EMAILS";
    const NOMINATE_OVERRIDE_EMAILS_ENV_KEY = "NOMINATE_OVERRIDE_EMAILS";
    const CONFIG_DATABASE_CONNECTION_FILE_NAME = 'yeltrik-teaching-honors-database-connections.php';
    const PUBLISHES_GROUP = 'teachingHonors';
    const VIEW_NAMESPACE = 'teachingHonors';

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        if (file_exists(config_path(static::CONFIG_DATABASE_CONNECTION_FILE_NAME))) {
            // User Customizable
            $this->mergeConfigFrom(
                config_path(static::CONFIG_DATABASE_CONNECTION_FILE_NAME), 'database.connections'
            );
            //dd(config('database'));
        } elseif (file_exists(static::packageSrc() . 'config/' . static::CONFIG_DATABASE_CONNECTION_FILE_NAME)) {
            // Non Customizable
            $this->mergeConfigFrom(
                static::packageSrc() . 'config/' . static::CONFIG_DATABASE_CONNECTION_FILE_NAME, 'database.connections'
            );
            //dd(config('database'));
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(static::packageSrc() . 'database/migrations');
        $this->loadRoutesFrom(static::packageSrc() . 'routes/web.php');
        $this->loadViewsFrom(static::packageSrc() . 'resources/views', static::VIEW_NAMESPACE);

        if ($this->app->runningInConsole()) {
            $this->publishResources();
        }
        Gate::define('import', function ($user) {
            $emails = explode(",", env(static::ADMIN_EMAILS_ENV_KEY));
            if (empty($emails)) {
                dd('Need to Set ' . static::ADMIN_EMAILS_ENV_KEY . ' Addresses in .env');
            } else {
                $email = auth()->user()->email;
                return in_array(strtolower($email), $emails);
            }
            return FALSE;
        });
        Gate::define('nominate', function ($user) {
            if (NominationWindow::isOpen()) {
                return TRUE;
            } else {
                $emails = explode(",", env(static::NOMINATE_OVERRIDE_EMAILS_ENV_KEY));
                if (empty($emails)) {
                    dd('Need to Set ' . static::NOMINATE_OVERRIDE_EMAILS_ENV_KEY . ' Addresses in .env');
                } else {
                    $email = auth()->user()->email;
                    return in_array(strtolower($email), $emails);
                }
            }
        });

        Gate::define('report', function ($user) {
            $emails = explode(",", env(static::ADMIN_EMAILS_ENV_KEY));
            if (empty($emails)) {
                dd('Need to Set ' . static::ADMIN_EMAILS_ENV_KEY . ' Addresses in .env');
            } else {
                $email = auth()->user()->email;
                return in_array(strtolower($email), $emails);
            }
            return FALSE;
        });

        Gate::define('batchMail', function ($user) {
            $emails = explode(",", env(static::MAILER_ADMINS_EMAILS_ENV_KEY));
            if (empty($emails)) {
                dd('Need to Set ' . static::MAILER_ADMINS_EMAILS_ENV_KEY . ' Addresses in .env');
            } else {
                $email = auth()->user()->email;
                return in_array(strtolower($email), $emails);
            }
            return FALSE;
        });
    }

    protected static function packageSrc()
    {
        return __DIR__ . '/../../';
    }

    protected function publishResources()
    {
        // User Customizable
        $this->publishes([
            static::packageSrc() . 'config/' . static::CONFIG_DATABASE_CONNECTION_FILE_NAME => config_path(static::CONFIG_DATABASE_CONNECTION_FILE_NAME),
        ], 'config');

        $this->publishes([], static::PUBLISHES_GROUP);
    }

}
