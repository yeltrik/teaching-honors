<?php

namespace Yeltrik\TeachingHonors\app\mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Yeltrik\Profile\app\models\Profile;
use Yeltrik\TeachingHonors\app\models\Nominee;

class NomineeNominated extends Mailable
{
    use Queueable, SerializesModels;

    private Nominee $nominee;

    /**
     * Create a new message instance.
     *
     * @param Nominee $nominee
     */
    public function __construct(Nominee $nominee)
    {
        $this->nominee = $nominee;
        $this->from(env('MAILABLE_FROM'), env('MAILABLE_FROM_NAME'));
        $this->initSubject();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('teachingHonors::mail.nominee.nominated.body')
            ->with([
                'nomineeName' => $this->nomineeName(),
            ]);
    }

    /**
     * @return string
     */
    private function nomineeName()
    {
        if ( $this->nominee->profile instanceof Profile ) {
            foreach($this->nominee->profile->personalNames as $personalName){
                return $personalName->fullName;
            }
        }
        return "Outstanding Faculty";
    }

    /**
     * @return string
     */
    public function initSubject()
    {
        $this->subject = "Announcing Fall 2020 Teaching Honors Nominations";
    }

    /**
     *
     */
    public function initTo()
    {
        $nominee = $this->nominee;
        if ($nominee instanceof Nominee) {
            $profile = $nominee->profile;
            if ($profile instanceof Profile) {
                foreach ($profile->emails as $email) {
                    $this->to($email->email);
                }
            }
        } else {
            dd(['bad Nominee',
                $nominee]);
        }
    }
}
