<?php

namespace Yeltrik\TeachingHonors\app\mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Yeltrik\UniMbr\app\models\DepartmentHead;

class DepartmentNomineesNominated extends Mailable
{
    use Queueable, SerializesModels;

    private DepartmentHead $departmentHead;

    /**
     * Create a new message instance.
     *
     * @param DepartmentHead $departmentHead
     */
    public function __construct(DepartmentHead $departmentHead)
    {
        $this->departmentHead = $departmentHead;
        $this->from(env('MAILABLE_FROM'), env('MAILABLE_FROM_NAME'));
        $this->initSubject();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('teachingHonors::mail.department.nominee.nominated.body')
            ->with([
                'departmentHeadName' => $this->departmentHeadName(),
            ]);
    }

    /**
     * @return string
     */
    private function departmentHeadName()
    {
        return $this->departmentHead->member->name;
    }

    /**
     * @return string
     */
    private function departmentHeadEmail()
    {
        return $this->departmentHead->member->email;
    }

    /**
     * @return string
     */
    public function initSubject()
    {
        $this->subject = "Announcing Fall 2020 Teaching Honors Nominations";
    }

    /**
     *
     */
    public function initTo()
    {
        $this->to($this->departmentHeadEmail());
    }

}
