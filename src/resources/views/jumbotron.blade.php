<?php $id = 'jumbotron' ?>

<div
    role="button"
    class="jumbotron text-light"
    style=" background : #333;  cursor : pointer; "
    data-toggle="collapse"
    data-target="#{{$id}}"
    onclick="$('#{{$id}}').collapse('toggle');"
    aria-expanded="true"
>
    <h4 class="display-4">Teaching Honors</h4>

    <p class="lead">The Center for Innovative Teaching &amp; Learning recognizes and honors members of the WKU faculty
        who put in long hours and invest hard work to be effective and intentional teachers.</p>

    <p class="lead">CITL encourages nominations from students, faculty, and staff. </p>

{{--    @if(\Yeltrik\TeachingHonors\app\NominationWindow::isOpen())--}}
        @guest()
            <br>
            <a
                class="button btn-primary btn-lg btn-block text-center"
                href="{{route('login')}}"
            >
                Login to View Nominations
            </a>

        @endguest
{{--    @endif--}}

{{--    Inside Open Window--}}
{{--    Infer: (Outside Open Window)--}}
{{--    Window Opening Soon--}}
{{--    Window has Passed--}}

    @if(\Yeltrik\TeachingHonors\app\NominationWindow::isOpen())
        @include('teachingHonors::countdown-close')
    @elseif(\Yeltrik\TeachingHonors\app\NominationWindow::isOpeningSoon())
        @include('teachingHonors::countdown-open')
    @else
{{--        No Countdown to Show--}}
    @endif

    <div
        id="{{$id}}"
        class="collapse show"
    >
{{--        <div class="card-body">--}}
{{--            <p>Please note that nominations will be evaluated based upon substantive, specific examples. Therefore,--}}
{{--                generalized positive comments—while complimentary—do not aid in establishing how and why the nominee--}}
{{--                deserves an award.</p>--}}

{{--            <p>We will be sure to pass on all nominations to the nominees.</p>--}}
{{--        </div>--}}
    </div>

    @include('teachingHonors::jumbotron-note')

</div>
