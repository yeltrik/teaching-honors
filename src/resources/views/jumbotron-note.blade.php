@if(\Yeltrik\TeachingHonors\app\NominationWindow::isOpen())

@elseif(\Yeltrik\TeachingHonors\app\NominationWindow::hasPassed())
    <p class="mt-4">NOTE: The window to submit nominations has expired. Please check back next semester to nominate an outstanding
        teacher.</p>
@else
    <p class="mt-4">NOTE: The window to submit nominations has not yet opened. Please check back soon to nominate an outstanding teacher.</p>
@endif
