@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="list-group">
            <a href="#" class="list-group-item list-group-item-action active">
                Nominees
            </a>
            @foreach($nominees as $nominee)
                <a href="{{route('teaching-honors.nominees.show', $nominee)}}" class="list-group-item list-group-item-action">
                    @if($nominee->profile instanceof \Yeltrik\Profile\app\models\Profile)
                        {{$nominee->profile->personalNames->first()->fullName}}
                    @else
                        Nominee: {{$nominee->id}}
                    @endif

                    <div class="badge badge-primary float-right">
                        Nominations: {{$nominee->nominations->count()}}
                    </div>
                </a>
            @endforeach
        </div>

    </div>

@endsection
