<div class="card">
    <div class="card-body">
        <h5 class="card-title">Profile</h5>
        @foreach($personalNames as $personalName)
            <h6 class="card-subtitle mb-2 text-muted">{{$personalName->fullName}}</h6>
        @endforeach
        @foreach($emails as $email)
            <h6 class="card-subtitle mb-2 text-muted">{{$email->email}}</h6>
        @endforeach
    </div>
</div>
