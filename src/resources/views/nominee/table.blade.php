<?php $tableId = "nominees-table-" . rand(1000,9999);?>
<script>
    $(document).ready(function () {
        $.noConflict();
        $('#{{$tableId}}').DataTable();
    });
</script>

<table id="{{$tableId}}" class="table table-striped">
    <thead>
    <tr>
        <th scope="col">Nominee</th>
        <th scope="col">Nominations</th>
        <th scope="col">Profile Name</th>
        <th scope="col">Profile Email</th>
        <th scope="col">University Email</th>
        <th scope="col">Department</th>
        <th>Destroy</th>
    </tr>
    </thead>
    <tbody>
    @foreach($nominees as $nominee)
        @include('teachingHonors::nominee.tr')
    @endforeach
    </tbody>
</table>

@include('teachingHonors::nominee.script')
