<div class="card">
    <div class="card-body">
        <h5 class="card-title">Memberships</h5>
        <?php
        $faculty = \Yeltrik\UniMbr\app\models\Faculty::query()
            ->where('member_id', '=', $nominee->member()->id)
            ->first();
        ?>
        @if($faculty instanceof \Yeltrik\UniMbr\app\models\Faculty)
            <h6 class="card-subtitle mb-2 text-muted">
                Faculty
                <br>
                {{$faculty->department->name}}
                <br>
                {{$faculty->department->college->name}}
            </h6>
        @else
            {{$nominee->member()->id}}
        @endif
    </div>
</div>
