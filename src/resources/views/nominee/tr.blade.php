<tr>
    <th scope="row">
        <a href="{{route('teaching-honors.nominees.show', $nominee)}}" class="btn btn-primary">
            <i class="fas fa-chalkboard-teacher"></i>
        </a>
    </th>
    <td class="text-center">
        @if($nominee->nominations->count() > 0)
            <div

                @can('transferAny', \Yeltrik\TeachingHonors\app\models\Nominee::class)
                class="badge badge-primary"
                data-nominee_id="{{$nominee->id}}"
                draggable="true"
                ondragstart="drag(event)"
                ondrop="drop(event)"
                ondragover="allowDrop(event)"
                @endcan
            >
                {{$nominee->nominations->count()}}
            </div>
        @else
            <div
                @can('transferAny', \Yeltrik\TeachingHonors\app\models\Nominee::class)
                data-nominee_id="{{$nominee->id}}"
                ondrop="drop(event)"
                ondragover="allowDrop(event)"
                @endcan
            >
                {{$nominee->nominations->count()}}
            </div>
        @endif
    </td>
    <td>
        @if($nominee->profile instanceof \Yeltrik\Profile\app\models\Profile )
            @foreach($nominee->profile->personalNames as $personalName)
                {{$personalName->fullName}}
            @endforeach
        @else
            N/A
        @endif
    </td>
    <td>
        @if($nominee->profile instanceof \Yeltrik\Profile\app\models\Profile )
            @foreach($nominee->profile->emails as $email)
                {{$email->email}}
            @endforeach
        @else
            N/A
        @endif
    </td>
    <td>
        @if($nominee->profile instanceof \Yeltrik\Profile\app\models\Profile )
            @if($nominee->member() instanceof \Yeltrik\UniMbr\app\models\Member )
                {{$nominee->member()->email}}
            @endif
        @endif
    </td>
    <td>
        @if($nominee->profile instanceof \Yeltrik\Profile\app\models\Profile )
            @if($nominee->member() instanceof \Yeltrik\UniMbr\app\models\Member )
                <?php
                $faculty = \Yeltrik\UniMbr\app\models\Faculty::query()
                    ->where('member_id', '=', $nominee->member()->id)
                    ->first();
                ?>
                @if($faculty instanceof \Yeltrik\UniMbr\app\models\Faculty)
                    {{$faculty->department->name}}
                @else
                    {{$nominee->member()->id}}
                @endif
            @endif
        @endif
    </td>
    <td>
        @can('destroy', $nominee)
            <form action="{{ route('teaching-honors.nominees.destroy', $nominee) }}" method="POST">
                @method('DELETE')
                @csrf
                <button class="btn btn-danger">Destroy</button>
            </form>
        @endcan
    </td>
</tr>
