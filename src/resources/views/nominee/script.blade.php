<script>
    function allowDrop(ev) {
        ev.preventDefault();
    }

    function drag(ev) {
        ev.dataTransfer.setData("text", ev.target.getAttribute('data-nominee_id'));
    }

    function drop(ev) {
        ev.preventDefault();
        var dragId = ev.dataTransfer.getData("Text");
        var targetId = ev.target.getAttribute('data-nominee_id');
        let route = "{{ route('teaching-honors.nominees.index') }}";
        let count = "#";
        let from = dragId;
        let to = targetId;
        $message = "Are you Sure you want to transfer Nominations?";
        // $message = "Are you Sure you want to transfer " + count + " nominations from '" + from + "' to '" + to + "'?";
        var r = confirm($message);
        if (r == true) {
            window.open(route + "/" + dragId + "/nomination/transfer/" + targetId);
        }
    }
</script>
