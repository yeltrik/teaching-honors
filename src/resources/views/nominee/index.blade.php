@extends('layouts.app')

@section('content')
    <div class="container">
        @include('teachingHonors::nominee.table')
    </div>
@endsection
