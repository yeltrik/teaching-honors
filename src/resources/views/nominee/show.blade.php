@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="row mb-2">
            <div class="col-6">
                @include('teachingHonors::nominee.profile')
            </div>
            @if($nominee->profile instanceof \Yeltrik\Profile\app\models\Profile )
                @if($nominee->member() instanceof \Yeltrik\UniMbr\app\models\Member )
                    <div class="col-6">
                        @include('teachingHonors::nominee.membership')
                    </div>
                @endif
            @endif
        </div>

        @foreach($nominations as $nomination)
            <div class="card mb-2">
                <div class="card-header">
                    Nomination
                    <span class="badge badge-pill badge-light float-right">
                    {{$nomination->term->fullAbbreviation}}
                </span>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            @include('teachingHonors::nomination.reason.card')
                        </div>
                        <div class="col-6">
                            @include('teachingHonors::nomination.example.card')
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
