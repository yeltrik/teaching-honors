<div class="py-5">
    <div class="row">
        <div class="col-lg-8 mx-auto">
            <div class="rounded bg-gradient-3 text-white shadow p-5 text-center mb-5">
                <p class="mb-0 font-weight-bold text-uppercase">Call for Nominations close in</p>
                <div id="clock" class="countdown pt-4"></div>
            </div>

        </div>
    </div>
</div>


<style id="compiled-css" type="text/css">
    .countdown {
        text-transform: uppercase;
        font-weight: bold;
    }

    .countdown span {
        text-shadow: 1px 1px 3px rgba(0, 0, 0, 0.1);
        font-size: 3rem;
        margin-left: 0.8rem;
    }

    .countdown span:first-of-type {
        margin-left: 0;
    }

    .countdown-circles span {
        width: 100px;
        height: 100px;
        border-radius: 50%;
        background: rgba(255, 255, 255, 0.2);
        display: flex;
        align-items: center;
        justify-content: center;
        box-shadow: 2px 2px 10px rgba(0, 0, 0, 0.1);
    }

    .countdown-circles span:first-of-type {
        margin-left: 0;
    }

    body {
        min-height: 100vh;
    }

    .bg-gradient-3 {
        background: #ff416c;
        background: -webkit-linear-gradient(to right, #ff416c, #ff4b2b);
        background: linear-gradient(to right, #ff416c, #ff4b2b);
    }

    .rounded {
        border-radius: 1rem !important;
    }


</style>

<script>
    jQuery(document).ready(function ($) {
        $('#clock').countdown('{{\Yeltrik\TeachingHonors\app\NominationWindow::nextClose()}}').on('update.countdown', function (event) {
            var $this = $(this).html(event.strftime(''
                + '<span class="h1 font-weight-bold">%D</span> Day%!d'
                + '<span class="h1 font-weight-bold">%H</span> Hr'
                + '<span class="h1 font-weight-bold">%M</span> Min'
                + '<span class="h1 font-weight-bold">%S</span> Sec'));
        });
    });

</script>
