<?php $tableId = "department-head-table-" . rand(1000, 9999); ?>

<script>
    $(document).ready(function () {
        @once
            $.noConflict();
        @endonce
        $('#{{$tableId}}').DataTable();
    });
</script>

<table id="{{$tableId}}" class="table table-striped">
    <thead>
    <tr>
        <th scope="col">Department Head</th>
        <th scope="col">Name</th>
        <th scope="col">Email</th>
        <th scope="col">Department</th>
        <th scope="col">Nominees and Nominations</th>
    </tr>
    </thead>
    <tbody>
    @foreach($departmentHeads as $departmentHead)
        @include('teachingHonors::department-head.tr')
    @endforeach
    </tbody>
</table>

@once
    @include('teachingHonors::nominee.script')
@endonce
