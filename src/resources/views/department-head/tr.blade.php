<tr>
    <th scope="row" class="text-center">
        <a href="#" class="btn btn-primary disabled">
            <i class="fas fa-university"></i>
        </a>
    </th>
    <td>
        @if($departmentHead->member instanceof \Yeltrik\UniMbr\app\models\Member )
            {{$departmentHead->member->name}}
        @endif
    </td>
    <td>
        @if($departmentHead->member instanceof \Yeltrik\UniMbr\app\models\Member )
            {{$departmentHead->member->email}}
        @endif
    </td>
    <td>
        @if($departmentHead->department instanceof \Yeltrik\UniOrg\app\models\Department)
            {{$departmentHead->department->name}}
        @else
            {{$nominee->member()->id}}
        @endif
    </td>
    <td class="text-center">
        <a href="{{route("reports.teaching-honors.departments.show", $departmentHead->department)}}" class="btn btn-primary">
            <i class="fas fa-chalkboard-teacher"></i>
        </a>
    </td>
</tr>
