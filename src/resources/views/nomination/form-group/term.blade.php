@if(\Yeltrik\TeachingHonors\app\NominationWindow::openTerm() instanceof \Yeltrik\UniTrm\app\models\Term)
    <input
        type="hidden"
        name="term_id"
        value="{{\Yeltrik\TeachingHonors\app\NominationWindow::openTerm()->id}}"
    ></input>
@else
    <div class="shadow-sm form-group p-2">
        <label for="term_id">
            Term
        </label>

        <select
            class="form-control"
            name="term_id"
            id="term_id"
            required
        >
            @foreach(\Yeltrik\UniTrm\app\models\Term::all() as $term)
                <option value="{{$term->id}}">
                    {{$term->fullAbbreviation}}
                </option>
            @endforeach
        </select>
    </div>

@endif
