<div class="shadow-sm form-group p-2">

    <label>
        Which of the following best describes the teacher you’re nominating?
    </label>

    @error('reason_ids')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <small class="form-text text-muted">
        (Select all that apply.)
    </small>
    @include('teachingHonors::nomination.form-control.reasons')
</div>
