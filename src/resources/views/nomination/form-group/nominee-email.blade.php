<div class="form-group">
    <label>Nominee's email</label>
    <input type="text"
           id="nominee_email"
           name="nominee_email"
           class="form-control"
           placeholder="Nominee's Email"
           value="{{trim(old('nominee_email'))}}"
    >
</div>
