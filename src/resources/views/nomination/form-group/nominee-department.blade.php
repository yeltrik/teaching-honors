<div class="form-group">
    <label>Nominee's Department</label>
    <select
        name="nominee_department"
        id="nominee_department"
        class="form-control chosen-select"
    >
        <option value="">Choose a Department...</option>

        @foreach($departments as $department)
            <option
                @if (old('nominee_department') == $department->name )
                selected
                @endif
            >{{$department->name}}</option>
        @endforeach

    </select>
</div>
