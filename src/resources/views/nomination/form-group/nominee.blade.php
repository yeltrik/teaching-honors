<div class="shadow-sm form-group p-2">
    <label for="nominee_select">
        Nominee
    </label>

    @if( !$nomineeSelectOptions->empty() )
        @include('teachingHonors::nomination.form-group.nominee-lookup-method', ['value' => 'true'])
        @include('teachingHonors::nomination.nav.nominee')
        @include('teachingHonors::nomination.tab-content.nominee')
    @else
        @include('teachingHonors::nomination.form-group.nominee-lookup-method', ['value' => 'false'])
        @include('teachingHonors::nomination.form-control.nominee-manual')
        <script>
            $(document).ready( function () {
                toggleNomineeRequired(false, true);
            });
        </script>
    @endif

</div>

<script>
    function toggleNomineeRequired(left, right) {
        $('#nominee_lookup_method').val(left);

        $('#nominee_select').prop('required', left);

        $('#nominee_first_name').prop('required', right);
        $('#nominee_last_name').prop('required', right);
        $('#nominee_email').prop('required', right);
        $('#nominee_college').prop('required', right);
        $('#nominee_department').prop('required', right);
    }
</script>
