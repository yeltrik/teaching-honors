<div class="shadow-sm form-group p-2">

    @error('nomination_example')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <label>
        In what specific ways has your nominee helped you be successful in your course?<br>
        Can you provide an example of how they created a good learning environment for you and your peers during
        the COVID-19 pandemic?
    </label>
    @include('teachingHonors::nomination.form-control.example')
</div>
