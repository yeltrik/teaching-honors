<div class="form-group">
    <label>Nominee's First Name</label>
    <small id="B7-6DD2-F1E7A69C" class="form-text text-muted"></small>
    <input type="text"
           placeholder="Nominee's First Name"
           name="nominee_first_name"
           id="nominee_first_name"
           class="form-control"
           value="{{trim(old('nominee_first_name'))}}"
    >
    <div class="invalid-feedback"></div>
</div>
