<div class="form-group">
    <label>Nominee's College</label>
    <select
        name="nominee_college"
        id="nominee_college"
        class="form-control chosen-select"
    >
        <option value="">Choose a College...</option>

        @foreach($colleges as $college)
            <option
                @if (old('nominee_college') == $college->name )
                selected
                @endif
            >{{$college->name}}</option>
        @endforeach

    </select>
</div>
