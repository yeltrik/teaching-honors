<form
    method="POST"
    action="{{ action([\Yeltrik\TeachingHonors\app\http\controllers\NominationController::class, 'store']) }}"
>
    @csrf

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @include('teachingHonors::nomination.form-group.term')

    @include('teachingHonors::nomination.form-group.nominee')
    @include('teachingHonors::nomination.form-group.reasons')
    @include('teachingHonors::nomination.form-group.example')

    @include('teachingHonors::nomination.card.header.nominator-info')
    @include('teachingHonors::nomination.card.body.nominator-info')

    <br>
    <label
        class="d-block mb-4"
        for=""
    >
        Please verify all information is correct, then click "Submit" below.
    </label>
    @include('teachingHonors::nomination.button.row')

</form>
