<input type="hidden" name="nominator_profile_id" value="{{$nominatorProfile->id}}">

<div id="21-D467-12B6C947" class="shadow-lg collapse" style="padding: 10px 10px 0px;" aria-labelledby="" data-parent="">
    <div class="form-row">
        <div class="col-md-6 mb-3">
            @include('teachingHonors::nomination.card.body.form-group.first-name')
        </div>

        <div class="col-md-6 mb-3">
            @include('teachingHonors::nomination.card.body.form-group.last-name')
        </div>
    </div>

    <div class="form-row">
        <div class="col-md-6 mb-3">
            @include('teachingHonors::nomination.card.body.form-group.email')
        </div>

        {{--<div class="col-md-6 mb-3">--}}
        {{--    @include('teachingHonors::nomination.card.body.form-group.relationship')--}}
        {{--</div>--}}
    </div>
</div>
