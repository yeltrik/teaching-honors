<div class="form-group">
    <label>Your relationship to the University</label>
    <small id="78-F43F-66C37506" class="form-text text-muted"></small>
    <div class="form-check">
        <input type="radio" name="nominator_relationship" value="Student" id="74-0217-45BC8349"
               class="form-check-input">

        <label class="form-check-label">Student</label>
    </div>
    <div class="form-check">
        <input type="radio" name="nominator_relationship" value="Faculty" id="24-AE4A-572FC9EE"
               class="form-check-input">

        <label class="form-check-label">Faculty</label>
    </div>
    <div class="form-check">
        <input type="radio" name="nominator_relationship" value="Staff" id="A7-CD36-F515F349"
               class="form-check-input">

        <label class="form-check-label">Staff</label>
    </div>
</div>
