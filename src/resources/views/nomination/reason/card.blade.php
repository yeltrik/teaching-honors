<div class="card">
    <div class="card-header">
        Reasons:
    </div>
    <ul class="list-group list-group-flush">
        @foreach($nomination->reasons as $reason)
            <li class="list-group-item">
                <i class="fas fa-check"></i>
                {{$reason->text}}
            </li>
        @endforeach
    </ul>
</div>
