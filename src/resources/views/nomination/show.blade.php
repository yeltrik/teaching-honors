@extends('layouts.app')

@section('content')
    <div class="container">


        <div class="card">
            <div class="card-header">
                Nomination for {{$nomineeName}}

                <span class="badge badge-pill badge-light float-right">
                    {{$term->fullAbbreviation}}
                </span>
            </div>
            <div class="card-body">

                <div class="card">
                    <div class="card-header">
                        Reasons:
                    </div>
                    <ul class="list-group list-group-flush">
                        @foreach($nomination->reasons as $reason)
                            <li class="list-group-item">
                                <i class="fas fa-check"></i>
                                {{$reason->text}}</li>
                        @endforeach
                    </ul>
                </div>

                <div class="card mt-4">
                    <div class="card-header">
                        Example
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">{{$nomination->example->example}}</li>
                    </ul>
                </div>

            </div>
        </div>

    </div>
@endsection
