<div class="form-group shadow-sm" style=" padding : 10px; ">
    <div class="row">
        <div class="col-md-6 mb-3">
            @include('teachingHonors::nomination.form-group.nominee-first-name')
        </div>

        <div class="col-md-6 mb-3">
            @include('teachingHonors::nomination.form-group.nominee-last-name')
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 mb-3">
            @include('teachingHonors::nomination.form-group.nominee-email')
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 mb-3">
            @include('teachingHonors::nomination.form-group.nominee-college')
        </div>
        <div class="col-md-6 mb-3">
            @include('teachingHonors::nomination.form-group.nominee-department')
        </div>
    </div>

</div>
