@foreach($reasonRadios as $key => $reasonRadio)
    <div class="form-check">
        <input type="checkbox"
               name="reason_ids[]"
               value="{{$key}}"
               id="{{\Yeltrik\TeachingHonors\app\form\ReasonRadios::$reasonValuePrefix}}{{$key}}"
               @if(is_array(old('reason_ids')) && in_array($key, old('reason_ids')))
               checked
               @endif
        >

        <label for="{{\Yeltrik\TeachingHonors\app\form\ReasonRadios::$reasonValuePrefix}}{{$key}}"
               class="form-check-label">
            {{$reasonRadio}}
        </label>
    </div>
@endforeach



