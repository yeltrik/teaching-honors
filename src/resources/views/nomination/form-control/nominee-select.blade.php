<div class="shadow-sm form-group p-2">

    @error('nominee_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <label>
        Nominee
    </label>

    <script>
        $(document).ready(function () {
            $.noConflict();
            $(".chosen-select").chosen();
        });
    </script>

    <select
        id="nominee_select"
        name="nominee_id"
        class="form-control chosen-select"
        data-placeholder="Choose a Faculty to Nominate"
        {{--oninvalid="this.setCustomValidity('Please select an Nominee from the list.')"--}}
        {{--oninput="setCustomValidity('')"--}}
        required
    >
        <option value="">Choose a Faculty to Nominate</option>

        @foreach($nomineeSelectOptions as $key => $nomineeSelectOption)
            <option
                value="{{$key}}"
                @if (old('nominee_id') == $key )
                selected
                @endif
            >
                {{$nomineeSelectOption}}
            </option>
        @endforeach

    </select>

</div>

