<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade mt-2 show active" id="nav-search" role="tabpanel" aria-labelledby="nav-search-tab">
        @include('teachingHonors::nomination.form-control.nominee-select')
    </div>
    <div class="tab-pane fade mt-2" id="nav-lookup" role="tabpanel" aria-labelledby="nav-lookup-tab">
        @include('teachingHonors::nomination.form-control.nominee-manual')
    </div>
</div>
