<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link w-75 active" id="nav-search-tab" data-toggle="tab" href="#nav-search" role="tab"
           aria-controls="nav-search" aria-selected="true"
           onclick="toggleNomineeRequired(true, false);"
        >
            <i class="font-awesome mr-2 fas fa-search"></i>
            Lookup
        </a>
        <a class="nav-item nav-link w-25" id="nav-lookup-tab" data-toggle="tab" href="#nav-lookup" role="tab"
           aria-controls="nav-lookup" aria-selected="false"
           onclick="toggleNomineeRequired(false, true);"
        >
            <i class="font-awesome mr-2 far fa-keyboard"></i>
            Manual
        </a>
    </div>
</nav>
