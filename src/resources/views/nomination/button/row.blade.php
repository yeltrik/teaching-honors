<div class="row no-gutters justify-content-center">
    <div class="col col-6 col-sm-8">
        @include('teachingHonors::nomination.button.submit')
    </div>
    <div class="col col-6 col-sm-4">
        @include('teachingHonors::nomination.button.reset')
    </div>
</div>
