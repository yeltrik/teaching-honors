<div class="py-5">
    <div class="row">
        <div class="col-lg-8 mx-auto">
            <div class="rounded bg-gradient-1 text-white shadow p-5 text-center mb-5">
                <p class="mb-4 font-weight-bold text-uppercase">Nominate an Outstanding Teacher in</p>
                <div id="clock-b" class="countdown-circles d-flex flex-wrap justify-content-center pt-4"></div>
            </div>
        </div>
    </div>
</div>

<style id="compiled-css" type="text/css">

    .countdown span {
        text-shadow: 1px 1px 3px rgba(0, 0, 0, 0.1);
        font-size: 3rem;
        margin-left: 0.8rem;
    }

    .countdown span:first-of-type {
        margin-left: 0;
    }

    .countdown-circles {
        text-transform: uppercase;
        font-weight: bold;
    }

    .countdown-circles span {
        width: 100px;
        height: 100px;
        border-radius: 50%;
        background: rgba(255, 255, 255, 0.2);
        display: flex;
        align-items: center;
        justify-content: center;
        box-shadow: 2px 2px 10px rgba(0, 0, 0, 0.1);
    }

    .countdown-circles span:first-of-type {
        margin-left: 0;
    }


    body {
        min-height: 100vh;
    }

    .bg-gradient-1 {
        background: #7f7fd5;
        background: -webkit-linear-gradient(to right, #7f7fd5, #86a8e7, #91eae4);
        background: linear-gradient(to right, #7f7fd5, #86a8e7, #91eae4);
    }

    .rounded {
        border-radius: 1rem !important;
    }

</style>

<script>
    jQuery(document).ready(function ($) {
        $('#clock-b').countdown('{{\Yeltrik\TeachingHonors\app\NominationWindow::nextOpen()}}').on('update.countdown', function (event) {
            var $this = $(this).html(event.strftime(''
                + '<div class="holder m-2"><span class="h1 font-weight-bold">%D</span> Day%!d</div>'
                + '<div class="holder m-2"><span class="h1 font-weight-bold">%H</span> Hr</div>'
                + '<div class="holder m-2"><span class="h1 font-weight-bold">%M</span> Min</div>'
                + '<div class="holder m-2"><span class="h1 font-weight-bold">%S</span> Sec</div>'));
        });
    });

</script>
