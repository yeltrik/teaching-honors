@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="list-group">
            <a href="#" class="list-group-item list-group-item-action active">
                Import
            </a>

            @if (Route::has('imports.teaching-honors.create'))
                <a href="{{route('imports.teaching-honors.create')}}" class="list-group-item list-group-item-action">Nominations</a>
            @endif

            @if (Route::has('imports.teaching-honors.faculty'))
                <a href="{{route('imports.teaching-honors.faculty')}}" class="list-group-item list-group-item-action">
                    Sync Faculty to Nominee
                </a>
            @endif

        </div>

    </div>
@endsection
