<form
    method="POST"
    action="{{ action([\Yeltrik\TeachingHonors\app\http\controllers\ImportTeachingHonorsController::class, 'store']) }}"
    enctype="multipart/form-data"
>
    @csrf

    @include('teachingHonors::import.teaching-honors.inputs.import_by_export_json_file')
    <br>

    <br>
    @include('teachingHonors::import.teaching-honors.inputs.import_new_nominations')
    <br>
    @include('teachingHonors::import.teaching-honors.inputs.update_existing_nominations')
    <br>

    <br>
    @include('teachingHonors::import.teaching-honors.inputs.import_button')

</form>
