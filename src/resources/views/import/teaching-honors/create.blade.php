@extends('layouts.app')

@section('content')
    <div class="container">

        @include('teachingHonors::import.teaching-honors.form')

    </div>
@endsection
