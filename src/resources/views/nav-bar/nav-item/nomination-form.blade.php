@can('nominate')
    @if (Route::has('teaching-honors.nominations.create'))
        <li class="nav-item {{ Route::currentRouteNamed('teaching-honors.nominations.create') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('teaching-honors.nominations.create') }}">
                Nomination Form
            </a>
        </li>
    @endif
@endcan
