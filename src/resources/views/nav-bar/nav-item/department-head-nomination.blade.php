@auth()
    @if (Route::has('reports.teaching-honors.departments.show'))
        @if(auth()->user()->isDepartmentHead())
            <li class="nav-item {{ Route::currentRouteNamed('reports.teaching-honors.departments.show') ? 'active' : '' }}">
                <a class="nav-link"
                   href="{{ route('reports.teaching-honors.departments.show', auth()->user()->departmentHead()->department) }}">
                    My Department
                </a>
            </li>
        @endif
    @endif
@endauth
