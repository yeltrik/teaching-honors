@auth()
    @if (Route::has('teaching-honors.users.nominations.index'))
        @if(\Yeltrik\TeachingHonors\app\models\Nomination::forUser(auth()->user()) !== NULL && \Yeltrik\TeachingHonors\app\models\Nomination::forUser(auth()->user())->count() > 0)
            <li class="nav-item {{ Route::currentRouteNamed('teaching-honors.users.nominations.index') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('teaching-honors.users.nominations.index', auth()->user()) }}">
                    My Nominations
                </a>
            </li>
        @endif
    @endif
@endauth
