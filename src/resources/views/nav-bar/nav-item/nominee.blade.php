@can('viewAny', \Yeltrik\TeachingHonors\app\models\Nominee::class)
    @if (Route::has('teaching-honors.nominees.index'))
        <li class="nav-item {{ Route::currentRouteNamed('teaching-honors.nominees.index') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('teaching-honors.nominees.index') }}">
                Nominees
            </a>
        </li>
    @endif
@endcan
