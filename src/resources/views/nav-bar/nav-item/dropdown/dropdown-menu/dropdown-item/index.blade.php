@can('import')
    @if (Route::has('imports.teaching-honors.index'))
        <a class="dropdown-item" href="{{ route('imports.teaching-honors.index') }}">Import Teaching Honors</a>
    @endif
@endcan
