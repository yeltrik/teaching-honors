@can('report')
    @if (Route::has('reports.teaching-honors.index'))
        <a class="dropdown-item" href="{{ route('reports.teaching-honors.index') }}">Teaching Honors</a>
    @endif
@endcan
