@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="list-group">
            <a href="#" class="list-group-item list-group-item-action active">
                Teaching Honors
            </a>
            <a href="{{route('reports.teaching-honors.colleges.index')}}"
               class="list-group-item list-group-item-action">
                By College/Department
            </a>
            <a href="{{route('reports.teaching-honors.terms.index')}}" class="list-group-item list-group-item-action">By
                Term
            </a>
            @can('viewAny', \Yeltrik\TeachingHonors\app\models\Nominee::class)
                <a href="{{route('teaching-honors.nominees.index')}}" class="list-group-item list-group-item-action">Nominees</a>
            @endcan
            @can('viewAny', \Yeltrik\TeachingHonors\app\models\Nominator::class)
                <a href="{{route('teaching-honors.nominators.index')}}" class="list-group-item list-group-item-action">Nominators</a>
            @endcan
            @can('batchMail')
                <a href="{{route('mails.teaching-honors.nominees.nominated.index')}}"
                   class="list-group-item list-group-item-action">
                    Batch Mail Nominees
                </a>
                <a href="{{route('mails.teaching-honors.departments.nominees.nominated.index')}}"
                   class="list-group-item list-group-item-action">
                    Batch Mail Department Heads
                </a>
            @endcan
        </div>

    </div>
@endsection
