<div class="card w-100" style="width: 18rem;">
    <div class="card-header">
        Overview

        <div class="float-right">
            @include('teachingHonors::report.teaching-honors.uni-trm.select')
        </div>
    </div>
    <ul class="list-group list-group-flush">
        <li class="list-group-item">
            @include('teachingHonors::report.teaching-honors.grid.all')
            <div class="float-right">
            </div>
        </li>
    </ul>
</div>

