<canvas id="myChart3"></canvas>
<script>
    $(document).ready(function () {
        var ctx = document.getElementById('myChart3').getContext('2d');
        var chart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: {!!json_encode($labels)!!},
                datasets:
                    {!!json_encode($datasets)!!}
            },
            options: {
                cutoutPercentage: 5,
                aspectRatio: 1,
                title: {
                    display: true,
                    text: 'Outstanding Teachers'
                },
                legend: {
                    display: false,
                },
                onClick: chartClickEvent
            }
        });

        function chartClickEvent(evt) {
            var activePointLabel = this.getElementsAtEvent(evt)[0]._model.label;
            //alert(activePointLabel);
            // var activePointLabel = this.getElementsAtEvent(evt)[0]._model.label;
            console.log(this.getElementsAtEvent(evt)[0]._model);
            let route = "{{ route('teaching-honors.nominees.index') }}";
            window.open(route+"/lookup/" + activePointLabel);
        }

    });
</script>
