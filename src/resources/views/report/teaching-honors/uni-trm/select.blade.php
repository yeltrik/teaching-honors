<label for="report-pd-college-uni-trm-select">
    Select Term
</label>
<select
    id="report-pd-college-uni-trm-select"
    onchange="selectTerm(this);"
>
    <option value="all">
        All
    </option>
    @foreach($terms as $term1)
        <option
            value="{{$term1->id}}"
            @if(isset($term) && $term->id === $term1->id) selected @endif
        >
            {{$term1->fullAbbreviation}}
        </option>
    @endforeach
</select>

<script>
    function selectTerm(element) {
        let val = $(element).val();

        let route = "{{ route('reports.teaching-honors.index') }}";

        if (val) {
            if (val == "all") {
                // This is loosing the /subfolder/ in front of the route
                console.log("Redirecting to "+ route+"/term/");
                window.location = route+"/term/";
            } else {
                window.location = route+"/term/" + val;
            }
        }
        return false;
    }
</script>
