@extends('layouts.app')

@section('content')
    <div class="container">

        @include('teachingHonors::report.teaching-honors.overview')

    </div>
@endsection
