@extends('layouts.app')

@section('content')
    <div class="container">

        <h1>Teaching Honors</h1>
        <h2>{{$college->name}}</h2>
        <div class="list-group">
            <a href="#" class="list-group-item list-group-item-action active">
                Departments
            </a>

            @foreach($departments as $department)
                <a href="{{route('reports.teaching-honors.departments.show', [$department])}}"
                   class="list-group-item list-group-item-action">{{$department->name}}</a>
            @endforeach
        </div>

    </div>
@endsection
