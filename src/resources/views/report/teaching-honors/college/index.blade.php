@extends('layouts.app')

@section('content')
    <div class="container">

        <h1>Teaching Honors</h1>

        <div class="list-group">
            <a href="#" class="list-group-item list-group-item-action active">
                Colleges
            </a>

            @foreach($colleges as $college)
                <a href="{{route('reports.teaching-honors.colleges.show', [$college])}}"
                   class="list-group-item list-group-item-action">{{$college->name}}</a>
            @endforeach
        </div>

    </div>
@endsection
