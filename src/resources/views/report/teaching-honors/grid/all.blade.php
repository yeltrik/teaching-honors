@if(empty($labels))
    <h3>There are no nominations during this period</H3>
@else
    @include('teachingHonors::report.teaching-honors.chart.outstanding')
@endif
