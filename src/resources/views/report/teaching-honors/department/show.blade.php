@extends('layouts.app')

@section('content')
    <div class="container">

        <h1>Teaching Honors</h1>
        <h2 class="ml-2">
            @can('view', $college)
                <a href="{{route('reports.teaching-honors.colleges.show', $college)}}">
                    {{$college->name}}
                </a>
            @else
                {{$college->name}}
            @endcan
        </h2>
        <h3 class="ml-3">{{$department->name}}</h3>

        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active w-50" id="nav-faculty-tab" data-toggle="tab" href="#nav-faculty"
                   role="tab"
                   aria-controls="nav-faculty" aria-selected="true">
                    Nominees
                    <div class="badge badge-primary ml-3">{{sizeof($nomineeNames)}}</div>
                </a>
                <a class="nav-item nav-link w-50" id="nav-nominations-tab" data-toggle="tab" href="#nav-nominations"
                   role="tab"
                   aria-controls="nav-nominations" aria-selected="false">
                    Nominations
                    <div class="badge badge-primary ml-3">{{sizeof($nominations)}}</div>
                </a>
            </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-faculty" role="tabpanel" aria-labelledby="nav-faculty-tab">
                <div class="list-group">
                    {{--                    <li class="list-group-item active">Faculty</li>--}}

                    @foreach($nomineeNames as $nomineeName)
                        <li class="list-group-item">{{$nomineeName}}</li>
                    @endforeach
                </div>
            </div>
            <div class="tab-pane fade" id="nav-nominations" role="tabpanel" aria-labelledby="nav-nominations-tab">

                <div class="list-group">
                    {{--                    <li class="list-group-item active">Nominations</li>--}}

                    @foreach($nominations as $nomination)
                        <li class="list-group-item">
                            <div class="card">
                                <div class="card-body">
                                    @can('viewNomineeName')
                                        <h5 class="card-title">{{$nomination->nominee->profile->personalNames->first()->fullName}}</h5>
                                    @endcan
                                    <h6 class="card-subtitle mb-2 text-muted">{{$nomination->term->fullAbbreviation}}</h6>
                                    <div class="row">
                                        <div class="col-6">
                                            <ul class="list-group mt-2">
                                                <li class="list-group-item active">Reasons</li>
                                                @foreach($nomination->reasons as $reason)
                                                    <li class="list-group-item">
                                                        {{$reason->text}}
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <div class="col-6">
                                            <h5 class="card-title">Example</h5>
                                            @can('viewNomineeName')
                                                <p class="card-text">{{$nomination->example->text}}</p>
                                            @else
                                                <p class="card-text">{{$nomination->example->anonymousExample}}</p>
                                            @endcan
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
