@if($departmentHeadsWithNominations->count() <= sizeof($departmentHeadEmails) && sizeof($departmentHeadsWithoutEmail) === 0)
    <a href="{{route('mails.teaching-honors.departments.nominees.nominated.mail')}}"
       class="btn btn-block btn-primary">
        Batch Send
    </a>
@else
    <h5>
        Unable to Send Emails until Department Heads have Emails associated
    </h5>
    <a href="#"
       class="btn btn-block btn-danger disabled"
    >
        Batch Send
    </a>
@endif
