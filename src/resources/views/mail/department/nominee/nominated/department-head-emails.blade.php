<ul class="list-group">
    <li class="list-group-item active">Emails</li>
    @foreach($departmentHeadEmails as $departmentHeadEmail)
        <li class="list-group-item">
            {{$departmentHeadEmail}}
        </li>
    @endforeach
</ul>
