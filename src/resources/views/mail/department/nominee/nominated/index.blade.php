@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="card">
            <div class="card-body">
                <h5 class="card-title">
                    Mailer: Department Heads with Nominated Nominees
                </h5>
                <h6 class="card-subtitle mb-2 text-muted">
                    Latest Term: {{$term->fullAbbreviation}}
                </h6>

                <div class="row">
                    <div class="nav flex-column nav-pills col-4" id="v-pills-tab" role="tablist"
                         aria-orientation="vertical">
                        <a class="nav-link active" id="v-pills-department-heads-tab" data-toggle="pill"
                           href="#v-pills-department-heads"
                           role="tab"
                           aria-controls="v-pills-department-heads" aria-selected="true">
                            Department Heads
                            <span class="badge badge-pill badge-primary float-right">
                                {{$departmentHeads->count()}}
                            </span>
                        </a>
                        <a class="nav-link" id="v-pills-department-heads-with-nominations-tab" data-toggle="pill"
                           href="#v-pills-department-heads-with-nominations"
                           role="tab"
                           aria-controls="v-pills-department-heads-with-nominations" aria-selected="true">
                            Department Heads (With Nominations)
                            <span class="badge badge-pill badge-primary float-right">
                                {{$departmentHeadsWithNominations->count()}}
                            </span>
                        </a>
                        <a class="nav-link" id="v-pills-department-head-emails-tab" data-toggle="pill"
                           href="#v-pills-department-head-emails"
                           role="tab"
                           aria-controls="v-pills-department-head-emails" aria-selected="false">
                            Department Head Emails
                            <span class="badge badge-pill badge-success float-right">
                                {{sizeof($departmentHeadEmails)}}
                            </span>
                        </a>
                        <a class="nav-link" id="v-pills-department-heads-without-email-tab" data-toggle="pill"
                           href="#v-pills-department-heads-without-email" role="tab"
                           aria-controls="v-pills-department-heads-without-email" aria-selected="false">
                            Department Heads without Email
                            <span class="badge badge-pill badge-danger float-right">
                                {{sizeof($departmentHeadsWithoutEmail)}}
                            </span>
                        </a>
                        <a class="nav-link" id="v-pills-department-heads-without-name-tab" data-toggle="pill"
                           href="#v-pills-department-heads-without-name" role="tab"
                           aria-controls="v-pills-department-heads-without-name" aria-selected="false">
                            Department Heads without Name
                            <span class="badge badge-pill badge-danger float-right">
                                {{sizeof($departmentHeadsWithoutName)}}
                            </span>
                        </a>
                        <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages"
                           role="tab"
                           aria-controls="v-pills-messages" aria-selected="false">
                            Preview
                        </a>
                    </div>

                    <div class="tab-content col-8" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-department-heads" role="tabpanel"
                             aria-labelledby="v-pills-department-heads-tab">
                            @include('teachingHonors::department-head.table')
                        </div>
                        <div class="tab-pane fade" id="v-pills-department-heads-with-nominations" role="tabpanel"
                             aria-labelledby="v-pills-department-heads-with-nominations-tab">
                            @include('teachingHonors::department-head.table', ['departmentHeads' => $departmentHeadsWithNominations])
                        </div>
                        <div class="tab-pane fade" id="v-pills-department-head-emails" role="tabpanel"
                             aria-labelledby="v-pills-department-head-emails-tab">
                            @include("teachingHonors::mail.department.nominee.nominated.department-head-emails")
                        </div>
                        <div class="tab-pane fade" id="v-pills-department-heads-without-email" role="tabpanel"
                             aria-labelledby="v-pills-department-heads-without-email-tab">
                            @include('teachingHonors::department-head.table', ['departmentHeads' => $departmentHeadsWithoutEmail])
                        </div>
                        <div class="tab-pane fade" id="v-pills-department-heads-without-name" role="tabpanel"
                             aria-labelledby="v-pills-department-heads-without-name-tab">
                            @include('teachingHonors::department-head.table', ['departmentHeads' => $departmentHeadsWithoutName])
                        </div>
                        <div class="tab-pane fade" id="v-pills-messages" role="tabpanel"
                             aria-labelledby="v-pills-messages-tab">
                            @include("teachingHonors::mail.department.nominee.nominated.send-button")
                            <hr>
                            {!! $preview !!}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
