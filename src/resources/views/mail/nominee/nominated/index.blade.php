@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="card">
            <div class="card-body">
                <h5 class="card-title">
                    Mailer: Nominated Nominees
                </h5>
                <h6 class="card-subtitle mb-2 text-muted">
                    Latest Term: {{$term->fullAbbreviation}}
                </h6>

                <div class="row">
                    <div class="nav flex-column nav-pills col-4" id="v-pills-tab" role="tablist"
                         aria-orientation="vertical">
                        <a class="nav-link active" id="v-pills-nominees-tab" data-toggle="pill"
                           href="#v-pills-nominees"
                           role="tab"
                           aria-controls="v-pills-nominees" aria-selected="true">
                            Nominees
                            <span class="badge badge-pill badge-primary float-right">
                                {{$nominees->count()}}
                            </span>
                        </a>
                        <a class="nav-link" id="v-pills-nominee-emails-tab" data-toggle="pill"
                           href="#v-pills-nominee-emails"
                           role="tab"
                           aria-controls="v-pills-nominee-emails" aria-selected="false">
                            Nominee Emails
                            <span class="badge badge-pill badge-success float-right">
                                {{sizeof($nomineeEmails)}}
                            </span>
                        </a>
                        <a class="nav-link" id="v-pills-nominees-without-email-tab" data-toggle="pill"
                           href="#v-pills-nominees-without-email" role="tab"
                           aria-controls="v-pills-nominees-without-email" aria-selected="false">
                            Nominees without Emails
                            <span class="badge badge-pill badge-danger float-right">
                                {{sizeof($nomineesWithoutEmail)}}
                            </span>
                        </a>
                        <a class="nav-link" id="v-pills-nominees-without-name-tab" data-toggle="pill"
                           href="#v-pills-nominees-without-name" role="tab"
                           aria-controls="v-pills-nominees-without-name" aria-selected="false">
                            Nominees without Names
                            <span class="badge badge-pill badge-danger float-right">
                                {{sizeof($nomineesWithoutName)}}
                            </span>
                        </a>
                        <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages"
                           role="tab"
                           aria-controls="v-pills-messages" aria-selected="false">
                            Preview
                        </a>
                    </div>

                    <div class="tab-content col-8" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-nominees" role="tabpanel"
                             aria-labelledby="v-pills-nominees-tab">
                            @include('teachingHonors::nominee.table')
                        </div>
                        <div class="tab-pane fade" id="v-pills-nominee-emails" role="tabpanel"
                             aria-labelledby="v-pills-nominee-emails-tab">
                            @include("teachingHonors::mail.nominee.nominated.nominee-emails")
                        </div>
                        <div class="tab-pane fade" id="v-pills-nominees-without-email" role="tabpanel"
                             aria-labelledby="v-pills-nominees-without-email-tab">
                            @include('teachingHonors::nominee.table', ['nominees' => $nomineesWithoutEmail])
                        </div>
                        <div class="tab-pane fade" id="v-pills-nominees-without-name" role="tabpanel"
                             aria-labelledby="v-pills-nominees-without-name-tab">
                            @include('teachingHonors::nominee.table', ['nominees' => $nomineesWithoutName])
                        </div>
                        <div class="tab-pane fade" id="v-pills-messages" role="tabpanel"
                             aria-labelledby="v-pills-messages-tab">
                            @include("teachingHonors::mail.nominee.nominated.send-button")
                            <hr>
                            {!! $preview !!}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
