<ul class="list-group">
    <li class="list-group-item active">Emails</li>
    @foreach($nomineeEmails as $nomineeEmail)
        <li class="list-group-item">
            {{$nomineeEmail}}
        </li>
    @endforeach
</ul>
