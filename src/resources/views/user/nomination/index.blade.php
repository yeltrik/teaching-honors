@extends('layouts.app')

@section('content')
    <div class="container">

        @if($nominations->count() === 0)
            @if(\Yeltrik\TeachingHonors\app\NominationWindow::isOpen())
                <h5 class="text-center mt-5">
                    You have not submitted any nominations.
                </h5>
                <a class="btn btn-primary btn-lg btn-block mt-4" href="{{route('teaching-honors.nominations.create')}}">
                    Click here to Nominate and outstanding teacher
                </a>
            @else
                @include('teachingHonors::jumbotron')
            @endif
        @else
            <h3>Nominations</h3>

            @if(\Yeltrik\TeachingHonors\app\NominationWindow::isOpen())
                <a class="btn btn-primary btn-block btn-lg mb-3" href="{{route('teaching-honors.nominations.create')}}">
                    Nominate another Outstanding Teacher
                </a>
            @endif

            @include('teachingHonors::user.nomination.list')
            {{--@include('teachingHonors::user.nomination.table')--}}

        @endif

    </div>
@endsection
