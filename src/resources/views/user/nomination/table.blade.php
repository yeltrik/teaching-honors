<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">Term</th>
        <th scope="col">Nominee</th>
        <th scope="col">Nominator</th>
        <th scope="col" width="35%">Reason(s)</th>
        <th scope="col" width="40%">Example(s)</th>
    </tr>
    </thead>
    <tbody>
    @foreach($nominations as $nomination)

        <tr>
            <th scope="row">
                {{$nomination->term->fullAbbreviation}}
            </th>
            <td>
                {{$nomination->nomineeIdentification}}
            </td>
            <td>
                {{$nomination->nominatorIdentification}}
            </td>
            <td>
                <ul class="list-group">
                    @foreach($nomination->reasons as $reason)
                        <li class="list-group-item">
                            <i class="fas fa-check"></i>
                            {{ $reason->text }} <br>

                        </li>
                    @endforeach
                </ul>
            </td>
            <td>
                {!! $nomination->example->example !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
