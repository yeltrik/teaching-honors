@foreach($nominations as $nomination)
    <div class="container">

        <div class="row">
            <div class="col col-3">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-subtitle text-muted float-left">Term</h6>
                        <h4 class="card-text float-right">
                            {{$nomination->term->fullAbbreviation}}
                        </h4>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-subtitle mb-2 text-muted float-left">Nominee</h6>
                        <h4 class="card-text float-right">
                            {{$nomination->nomineeIdentification}}
                        </h4>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-subtitle mb-2 text-muted float-left">Nominator</h6>
                        <h4 class="card-text float-right">
                            {{$nomination->nominatorIdentification}}
                        </h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col">
                <div class="card h-100">
                    <div class="card-body">
                        <h6 class="card-subtitle mb-2 text-muted">Reasons</h6>
                        <p class="card-text">
                        <ul class="list-group">
                            @foreach($nomination->reasons as $reason)
                                <li class="list-group-item">
                                    <i class="fas fa-check"></i>
                                    {{ $reason->text }} <br>

                                </li>
                            @endforeach
                        </ul>
                        </p>

                    </div>
                </div>

            </div>
            <div class="col">
                <div class="card h-100">
                    <div class="card-body">
                        <h6 class="card-subtitle mb-2 text-muted">Example</h6>
                        <p class="card-text">
                            {!! $nomination->example->example !!}

                        </p>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <hr>
@endforeach
