<?php $tableId = "nominators-table-" . rand(1000,9999);?>
<script>
    $(document).ready(function () {
        $.noConflict();
        $('#{{$tableId}}').DataTable();
    });
</script>

<table id="{{$tableId}}" class="table table-striped">
    <thead>
    <tr>
        <th scope="col">Nominations</th>
        <th scope="col">Profile Name</th>
        <th scope="col">Profile Email</th>
        <th scope="col">Terms</th>
    </tr>
    </thead>
    <tbody>
    @foreach($nominators as $nominator)
        @include('teachingHonors::nominators.tr')
    @endforeach
    </tbody>
</table>
