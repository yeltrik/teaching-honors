<tr>
    <td class="text-center">
        <div
            class="badge badge-primary"
        >
            {{$nominator->nominations->count()}}
        </div>
    </td>
    <td>
        @if($nominator->profile instanceof \Yeltrik\Profile\app\models\Profile )
            @foreach($nominator->profile->personalNames as $personalName)
                {{$personalName->fullName}}
            @endforeach
        @else
            N/A
        @endif
    </td>
    <td>
        @if($nominator->profile instanceof \Yeltrik\Profile\app\models\Profile )
            @foreach($nominator->profile->emails as $email)
                {{$email->email}}
            @endforeach
        @else
            N/A
        @endif
    </td>
    <td>
        @foreach($nominator->nomnationTerms() as $term)
            <span class="badge badge-pill badge-info">
            {{$term->fullAbbreviation}}
            </span>
        @endforeach
    </td>
</tr>
