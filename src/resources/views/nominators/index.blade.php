@extends('layouts.app')

@section('content')
    <div class="container">
        @include('teachingHonors::nominators.table')
    </div>
@endsection
